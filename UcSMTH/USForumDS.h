//
//  USForumDS.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-20.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "TTSectionedDataSource.h"

@interface USForumDS : TTSectionedDataSource
+ (id) dataSource;
@end

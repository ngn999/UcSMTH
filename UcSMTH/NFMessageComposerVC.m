//
// Created by kerberos on 12-8-24.
//
// To change the template use AppCode | Preferences | File Templates.
//

#import <ImageIO/ImageIO.h>
#import <CoreGraphics/CoreGraphics.h>
#import "UcBrewKit.h"
#import "NFMessageComposerVC.h"
#import "AGImagePickerController.h"
#import "USAjaxRequest.h"
#import "ATMHud.h"
#import "ATMHudDelegate.h"
#import "NFTokenFeild.h"
#import "SBJSON.h"

enum {
    kNFMessageComposerTagToEditor = 1000,
    kNFMessageComposerTagSubjectEditor = 1001,
    kNFMessageComposerTagAttachmentEditor = 1002,
    kNFMessageComposerTagBodyEditor = 1003,
};

@interface NFMessageComposerVC () <UITextFieldDelegate, UITextViewDelegate, NFTokenFieldDelegate,
        AGImagePickerControllerDelegate, ATMHudDelegate>
{
    struct {
        unsigned int to: 1;
        unsigned int subject: 1;
        unsigned int attachment: 1;
        unsigned int remain: 13;
    } _flags;
}
@property (nonatomic, assign) BOOL isViewLoaded;
@property (nonatomic, retain, readwrite) NSMutableArray* attachments;

@property (nonatomic, retain) UIScrollView* scrollView;
@property (nonatomic, retain) UIView* overlayView;
@property (nonatomic, retain) UITextField* toEditor;
@property (nonatomic, retain) UITextField* subjectEditor;
@property (nonatomic, retain) NFTokenField* attachmentEditor;
@property (nonatomic, retain) UIButton* attachmentAddButton;
@property (nonatomic, retain) UITextView* bodyEditor;
@property (nonatomic, retain) UIView* seperatorLine1;
@property (nonatomic, retain) UIView* seperatorLine2;
@property (nonatomic, retain) UIView* seperatorLine3;
@property (nonatomic, assign) CGFloat keyboardHeight;
@property (nonatomic, retain) ATMHud* atmHUD;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) SEL hudEndSEL;
@property (nonatomic, assign) BOOL isHudShow;
@end

@implementation NFMessageComposerVC
{

@private
    UITextField* _toEditor;
    UITextField* _subjectEditor;
    UITextView* _bodyEditor;
    NSString* _to;
    NSString* _subject;
    NSString* _body;
    NSMutableArray* _attachments;
    NFTokenField* _attachmentEditor;
    CGFloat _keyboardHeight;
    UIView* _seperatorLine1;
    UIView* _seperatorLine2;
    UIView* _seperatorLine3;
    UIScrollView* _scrollView;
    UIView* _overlayView;
    UIButton* _attachmentAddButton;
    id <NFMessageComposerVCDelegate> _delegate;
    ATMHud* _atmHUD;
    NSUInteger _maxOfAttachments;
    BOOL _isViewLoaded;
    NFMessageComposerHeaderFieldMask _headerFieldMask;
    BOOL _isLoading;
    SEL _hudEndSEL;
    BOOL _isHudShow;
}
@synthesize toEditor = _toEditor;
@synthesize subjectEditor = _subjectEditor;
@synthesize bodyEditor = _bodyEditor;
@synthesize attachmentEditor = _attachmentEditor;
@synthesize keyboardHeight = _keyboardHeight;
@synthesize seperatorLine1 = _seperatorLine1;
@synthesize seperatorLine2 = _seperatorLine2;
@synthesize seperatorLine3 = _seperatorLine3;
@synthesize scrollView = _scrollView;
@synthesize overlayView = _overlayView;
@synthesize attachmentAddButton = _attachmentAddButton;
@synthesize delegate = _delegate;
@synthesize atmHUD = _atmHUD;
@synthesize maxOfAttachments = _maxOfAttachments;
@synthesize isViewLoaded = _isViewLoaded;
@synthesize isLoading = _isLoading;
@synthesize hudEndSEL = _hudEndSEL;
@synthesize isHudShow = _isHudShow;





#pragma mark -
#pragma mark Public
+ (id) viewControllerWithBoardId: (NSString*) boardId articleId: (NSString*) articleId to: (NSString*) to
                         subject: (NSString*) subject body: (NSString*) body
                        delegate: (id <NFMessageComposerVCDelegate>) delegate
{
    return [[[NFMessageComposerVC alloc]
                                  initWithBoardId: boardId articleId: articleId to: to subject: subject body: body
                                         delegate: delegate]
                                  autorelease];
}

+ (id) viewControllerWithFieldMask: (NFMessageComposerHeaderFieldMask) mask
{
    return [[[NFMessageComposerVC alloc] initWithFiledMask: mask] autorelease];
}

- (id) initWithBoardId: (NSString*) boardId articleId: (NSString*) articleId to: (NSString*) to
               subject: (NSString*) subject body: (NSString*) body
              delegate: (id <NFMessageComposerVCDelegate>) delegate
{
    self = [super init];
    if (self) {
        _to = [to copy];
        _subject = [subject copy];
        _body = [body copy];
        _delegate = delegate;
        _headerFieldMask = NFMessageComposerHeaderFieldToMask | NFMessageComposerHeaderFieldSubjectMask
            | NFMessageComposerHeaderFieldAttachmentMask;
    }

    return self;
}

- (id) initWithFiledMask: (NFMessageComposerHeaderFieldMask) mask
{
    self = [super init];
    if (self) {
        _headerFieldMask = mask;
    }
    return self;
}


- (void) loadView
{
    [super loadView];
    [self setupViews];
    [self setupActions];
    [[NSNotificationCenter defaultCenter]
                           addObserver: self selector: @selector(keyboardDidShow:) name: UIKeyboardDidShowNotification
                                object: nil];

    [[NSNotificationCenter defaultCenter]
                           addObserver: self selector: @selector(keyboardWillHide:) name: UIKeyboardWillHideNotification
                                object: nil];
}

- (void) viewWillAppear: (BOOL) animated
{
    [super viewWillAppear: animated];
    [self layoutViews];
}

- (void) viewDidAppear: (BOOL) animated
{
    [super viewDidAppear: animated];
    [self showkeyboard];
}

- (void) viewWillDisappear: (BOOL) animated
{
    [self hideKeyboard];
    [super viewWillDisappear: animated];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.isViewLoaded = YES;
    self.headerFieldMask = _headerFieldMask;
}

- (void) viewDidUnload
{
    [super viewDidLoad];
    self.subjectEditor = nil;
    self.toEditor = nil;
    self.bodyEditor = nil;
    self.seperatorLine1 = nil;
    self.seperatorLine2 = nil;
    self.seperatorLine3 = nil;
    self.scrollView = nil;
    self.attachmentAddButton = nil;
}


- (void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    [_toEditor release], _toEditor = nil;
    [_subjectEditor release], _subjectEditor = nil;
    [_bodyEditor release], _bodyEditor = nil;
    [_to release], _to = nil;
    [_subject release], _subject = nil;
    [_body release], _body = nil;
    [_attachments release], _attachments = nil;
    [_attachmentEditor release], _attachmentEditor = nil;
    [_seperatorLine1 release], _seperatorLine1 = nil;
    [_seperatorLine2 release], _seperatorLine2 = nil;
    [_seperatorLine3 release], _seperatorLine3 = nil;
    [_scrollView release], _scrollView = nil;
    [_overlayView release], _overlayView = nil;
    [_attachmentAddButton release], _attachmentAddButton = nil;
    [_atmHUD release], _atmHUD = nil;
    [super dealloc];
}

#pragma mark -
#pragma mark Accessors

- (NFMessageComposerHeaderFieldMask) headerFieldMask
{
    NFMessageComposerHeaderFieldMask mask = NFMessageComposerHeaderFieldNone;
    if (_flags.to) {
        mask |= NFMessageComposerHeaderFieldToMask;
    }
    if (_flags.subject) {
        mask |= NFMessageComposerHeaderFieldSubjectMask;
    }

    if (_flags.attachment) {
        mask |= NFMessageComposerHeaderFieldAttachmentMask;
    }

    return mask;
}

- (NSString*) to
{
    if (_isViewLoaded) {
        return _toEditor.text;
    } else {
        return _to;
    }
}

- (void) setTo: (NSString*) to
{
    if (_to != to) {
        to = [to mutableCopy];
        [_to release];
        _to = to;
    }
    _toEditor.text = _to;
}

- (NSString*) subject
{
    if (_isViewLoaded) {
        return _subjectEditor.text;
    } else {
        return _subject;
    }
}

- (void) setSubject: (NSString*) subject
{
    if (_subject != subject) {
        subject = [subject mutableCopy];
        [_subject release];
        _subject = subject;
    }

    _subjectEditor.text = _subject;
}

- (NSString*) body
{
    if (_isViewLoaded) {
        return _bodyEditor.text;
    } else {
        return _body;
    }
}

- (void) setBody: (NSString*) body
{
    if (_body != body) {
        body = [body mutableCopy];
        [_body release];
        _body = body;
    }

    _bodyEditor.text = _body;
}

- (void) setHeaderFieldMask: (NFMessageComposerHeaderFieldMask) headerFieldMask
{
    _headerFieldMask = headerFieldMask;
    if (self.isViewLoaded) {
        if (headerFieldMask & NFMessageComposerHeaderFieldToMask) {
            _flags.to = 1;
            [_scrollView addSubview: _toEditor];
            [_scrollView addSubview: _seperatorLine1];
        } else {
            _flags.to = 0;
            [_toEditor removeFromSuperview];
            [_seperatorLine1 removeFromSuperview];
        }

        if (headerFieldMask & NFMessageComposerHeaderFieldSubjectMask) {
            _flags.subject = 1;
            [_scrollView addSubview: _subjectEditor];
            [_scrollView addSubview: _seperatorLine2];
        } else {
            _flags.subject = 0;
            [_subjectEditor removeFromSuperview];
            [_seperatorLine2 removeFromSuperview];
        }

        if (headerFieldMask & NFMessageComposerHeaderFieldAttachmentMask) {
            _flags.attachment = 1;
            [_scrollView addSubview: _attachmentEditor];
            [_scrollView addSubview: _seperatorLine3];
        } else {
            _flags.attachment = 0;
            [_attachmentEditor removeFromSuperview];
            [_seperatorLine3 removeFromSuperview];
        }
        [self layoutViews];
    }
}

- (NSArray*) attachments
{
    return _attachments;
}

#pragma mark -
#pragma mark Private

- (void) setupActions
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                                               initWithBarButtonSystemItem: UIBarButtonSystemItemDone
                                                                                    target: self
                                                                                    action: @selector(handleDoneButton)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
                                                              initWithBarButtonSystemItem: UIBarButtonSystemItemCancel target: self
                                                                                   action: @selector(handleCancelButton)];
}

- (void) setupViews
{
    self.view.backgroundColor = [UIColor whiteColor];

    UCBREW_LOG_OBJECT(NSStringFromCGRect (self.view.bounds));
    UCBREW_LOG_OBJECT(NSStringFromCGRect (self.view.frame));
    _scrollView = [[UIScrollView alloc] initWithFrame: self.view.bounds];
    [self.view addSubview: _scrollView];

    _toEditor = [[UITextField alloc] initWithFrame: CGRectZero];
    _toEditor.text = self.to;
    _toEditor.tag = kNFMessageComposerTagToEditor;
    _toEditor.delegate = self;
    _toEditor.placeholder = NSLocalizedString(@"Please input user id", nil);
    if ([_toEditor.leftView isKindOfClass: [UILabel class]]) {
        UILabel* v = (UILabel* )_toEditor.leftView;
        v.text = @"收件人: ";
    } else {
        UILabel* v = [[UILabel alloc] initWithFrame: CGRectZero];
        [v setBackgroundColor: [UIColor clearColor]];
        v.textColor = [UIColor colorWithRed: 0.3 green: 0.3 blue: 0.3
                                      alpha: 1.0];
        v.font = [UIFont systemFontOfSize: 17.0f];
        v.text = NSLocalizedString(@"To: ", nil);
        [v sizeToFit];
        UCBREW_LOG_OBJECT(NSStringFromCGRect (v.frame));
        _toEditor.leftView = v;
        _toEditor.leftViewMode = UITextFieldViewModeAlways;
        [v release];
    }
    [_scrollView addSubview: _toEditor];
    _seperatorLine1 = [[UIView alloc] initWithFrame: CGRectMake (0.0f, 0.0f, 320.0f, 1.0f)];
    _seperatorLine1.backgroundColor = [UIColor lightGrayColor];
    [_scrollView addSubview: _seperatorLine1];


    _subjectEditor = [[UITextField alloc] initWithFrame: CGRectMake (6.0f, 8.0f, 308.0f,
            20.0f)];
    _subjectEditor.font = [UIFont systemFontOfSize: 17.0f];
    _subjectEditor.tag = kNFMessageComposerTagSubjectEditor;
    _subjectEditor.delegate = self;
    _subjectEditor.text = self.subject;
    _subjectEditor.placeholder = NSLocalizedString(@"Please input subject", nil);
    if ([_subjectEditor.leftView isKindOfClass: [UILabel class]]) {
        UILabel* v = (UILabel* )_subjectEditor.leftView;
        v.text = @"主题: ";
        UCBREW_LOG(@"has label");
    } else {
        UCBREW_LOG(@"has label");
        UILabel* v = [[UILabel alloc] initWithFrame: CGRectZero];
        [v setBackgroundColor: [UIColor clearColor]];
        v.textColor = [UIColor colorWithRed: 0.3 green: 0.3 blue: 0.3
                                      alpha: 1.0];
        v.font = [UIFont systemFontOfSize: 17.0f];
        v.text = NSLocalizedString(@"Subject: ", nil);
        [v sizeToFit];
        UCBREW_LOG_OBJECT(NSStringFromCGRect (v.frame));
        _subjectEditor.leftView = v;
        _subjectEditor.leftViewMode = UITextFieldViewModeAlways;
        [v release];
    }
    [_scrollView addSubview: _subjectEditor];
    _seperatorLine2 = [[UIView alloc] initWithFrame: CGRectMake (0.0f, 0.0f, 320.0f, 1.0f)];
    _seperatorLine2.backgroundColor = [UIColor lightGrayColor];
    [_scrollView addSubview: _seperatorLine2];


    _attachmentEditor = [[NFTokenField alloc] initWithFrame: CGRectMake (0.0f, 0.0f,
        320.0f, 40.0f)];
    _attachmentEditor.delegate = self;
    _attachmentEditor.textLabel.textColor = [UIColor colorWithRed: 0.3 green: 0.3 blue: 0.3
                                  alpha: 1.0];
    _attachmentEditor.textLabel.font = [UIFont systemFontOfSize: 17.0f];
    _attachmentEditor.textLabel.text = NSLocalizedString(@"Attachment: ", nil);
    _attachmentEditor.backgroundColor = [UIColor clearColor];
    _attachmentEditor.accessoryType = NFTokenFeildAccessoryTypeAdd;
    [_scrollView addSubview: _attachmentEditor];
    _seperatorLine3 = [[UIView alloc] initWithFrame: CGRectMake (0.0f, 0.0f, 320.0f, 1.0f)];
    _seperatorLine3.backgroundColor = [UIColor lightGrayColor];
    [_scrollView addSubview: _seperatorLine3];

    _bodyEditor = [[UITextView alloc] initWithFrame: CGRectMake (0.0f, 0.0f, 320.0f,
            200.0f)];
    _bodyEditor.text = self.body;
    _bodyEditor.tag = kNFMessageComposerTagBodyEditor;
    _bodyEditor.font = [UIFont systemFontOfSize: 17.0];
    _bodyEditor.delegate = self;
    _bodyEditor.scrollEnabled = NO;
    _bodyEditor.delegate = self;

    [_scrollView addSubview: _bodyEditor];
    _atmHUD = [[ATMHud alloc] initWithDelegate: self];
    [self.view addSubview: _atmHUD.view];
}

- (void) layoutViews
{
    CGFloat top;
    CGRect frame_rect;
    top = 0;
    if (_flags.to) {
        top += 6;
        [_toEditor sizeToFit];
        frame_rect = _toEditor.frame;
        frame_rect.origin.x = 6;
        frame_rect.origin.y = top;
        frame_rect.size.width = self.view.bounds.size.width - 6 * 2;
        _toEditor.frame = frame_rect;
        top += frame_rect.size.height;

        frame_rect = _seperatorLine1.frame;
        frame_rect.size.width = self.view.bounds.size.width;
        top += 6;
        frame_rect.origin.y = top;
        _seperatorLine1.frame = frame_rect;

        top += 1.0;
    }

    if (_flags.subject) {
        top += 6;
        [_subjectEditor sizeToFit];
        frame_rect = _subjectEditor.frame;
        frame_rect.origin.x = 6;
        frame_rect.origin.y = top;
        frame_rect.size.width = self.view.bounds.size.width - 6 * 2;
        _subjectEditor.frame = frame_rect;
        top += frame_rect.size.height;

        frame_rect = _seperatorLine2.frame;
        frame_rect.size.width = self.view.bounds.size.width;
        frame_rect.origin.x = 0;
        top += 6;
        frame_rect.origin.y = top;
        _seperatorLine2.frame = frame_rect;
        top += 1.0;
    }

    if (_flags.attachment) {
        frame_rect = _attachmentEditor.frame;
        frame_rect.size = _attachmentEditor.contentSize;
        frame_rect.origin.x = 0;
        frame_rect.origin.y = top;
        _attachmentEditor.frame = frame_rect;
        top += frame_rect.size.height;

        frame_rect = _seperatorLine3.frame;
        frame_rect.origin.x = 0;
        top += 2;
        frame_rect.origin.y = top;
        frame_rect.size.width = self.view.bounds.size.width;
        _seperatorLine3.frame = frame_rect;
        top += 1;
    }

    frame_rect = _bodyEditor.frame;
    frame_rect.origin.y = top;
    frame_rect.size = _bodyEditor.contentSize;
    UCBREW_LOG (@"%f", _keyboardHeight);
    if (frame_rect.size.height < self.view.bounds.size.height - top - _keyboardHeight) {
        frame_rect.size.height = self.view.bounds.size.height - top - _keyboardHeight;
    }
    top += frame_rect.size.height;
    _bodyEditor.frame = frame_rect;
    [_scrollView setContentSize: CGSizeMake (320.0f, top)];
}

- (void) resizeViews
{
    [UIView beginAnimations: @"resize" context: nil];
    [UIView setAnimationDuration: 0.3f];
    CGRect frame_rect = self.view.bounds;
    frame_rect.size.height = self.view.frame.size.height - self.keyboardHeight;
    _scrollView.frame = frame_rect;
    [UIView commitAnimations];
    //[_bodyEditor setFrame:_attachmentEditor.contentView.bounds];
}

- (void) showkeyboard
{
    if (!(UcBrew_IsNotEmptyString (self.to)) && _flags.to) {
        [_toEditor becomeFirstResponder];
    }
    if (!(UcBrew_IsNotEmptyString (self.subject)) && _flags.subject) {
        [_subjectEditor becomeFirstResponder];
    } else {
        [_bodyEditor becomeFirstResponder];
        _bodyEditor.selectedRange = NSMakeRange (0, 0);
    }
}

- (void) hideKeyboard
{
    [self.scrollView scrollsToTop];
    [self.subjectEditor resignFirstResponder];
    [self.bodyEditor resignFirstResponder];
}

- (void) showHUD
{
    [_atmHUD setBlockTouches: YES];
    [_atmHUD setCaption: NSLocalizedString(@"Sending ...", nil)];
    [_atmHUD setActivity: YES];
    _isHudShow = YES;
    [_atmHUD show];
}

- (void) hideHUD
{
    [_atmHUD hideAfter: 2];
    [_atmHUD setBlockTouches: NO];
}

- (void) callDelegateCancel
{
    UCBREW_CALL_METHOD;
    if (self.delegate && [self.delegate respondsToSelector: @selector(messageComposerDidCancel:)]) {
        [self.delegate performSelector: @selector(messageComposerDidCancel:) withObject: self];
    } else {
        [self dismissModalViewControllerAnimated: YES];
    }
}

- (void) callDelegateFinish
{
    if (self.delegate && [self.delegate respondsToSelector: @selector(messageComposerDidFinish:)]) {
        [self.delegate performSelector: @selector(messageComposerDidFinish:) withObject: self afterDelay: 2];
    } else {
        [self dismissModalViewControllerAnimated: YES];
    }
}


#pragma mark -
#pragma mark Actions

- (void) handleDoneButton
{
    if (!_isLoading) {

        [self startSend];
    }
    return;
}

- (void) handleCancelButton
{
    if (!_isLoading) {
        [self startCancel];
    }
}

- (void) handleAttachmentAddButton: (id) sender
{
    UCBREW_CALL_METHOD;
    AGImagePickerController* image_controller = nil;
    image_controller = [[AGImagePickerController alloc] initWithDelegate: self];
    [self.navigationController presentModalViewController: image_controller
                                                 animated: YES];
    image_controller.maximumNumberOfPhotos = _maxOfAttachments;
    NSMutableArray* array = [NSMutableArray arrayWithCapacity: self.attachments.count];
    for (NSDictionary*  iter in self.attachments) {
        [array addObject: [iter objectForKey: kNFMessageComposerAttachementAsset]];
    }

    image_controller.selection = [NSArray arrayWithArray: array];
    [image_controller release];
}

#pragma mark -
#pragma Method

- (void) startSend
{
    UCBREW_CALL_METHOD;
    [self hideKeyboard];
    [self showHUD];
}

- (void) didFinishSend
{
    [_atmHUD setActivity: NO];
    [_atmHUD setCaption: @"Sucess"];
    [_atmHUD setImage: [UIImage imageNamed: @"success.png"]];
    [_atmHUD show];
    if (_isHudShow) {
        self.hudEndSEL = @selector(callDelegateFinish);
        [self hideHUD];
    } else {
        [self callDelegateFinish];
    }
    _isLoading = NO;
    /* call delegate */
}

- (void) startCancel
{
    UCBREW_CALL_METHOD;
    [self didFinishCancel];

}

- (void) didFinishCancel
{
    UCBREW_CALL_METHOD;
    if (_isHudShow) {
        self.hudEndSEL = @selector(callDelegateCancel);
        [self hideHUD];
    } else{
        [self callDelegateCancel];
    }
    _isLoading = NO;
    /* call delegate */
}

- (void) didFailWithError: (NSError*) error
{
    NSString* error_title = [error localizedFailureReason];
    //[self hideHUD];
    //[self showHUD];
    [_atmHUD setActivity: NO];
        UCBREW_LOG_OBJECT(error_title);
    [_atmHUD setCaption: error_title];
    [_atmHUD setImage: [UIImage imageNamed: @"error.png"]];
    [_atmHUD show];
    self.hudEndSEL = NULL;
    [self hideHUD];
    _isLoading = NO;
}


#pragma mark -
#pragma mark ATMHud

- (void) userDidTapHud: (ATMHud*) _hud
{
    [self hideHUD];
}

- (void) hudDidDisappear: (ATMHud*) _hud
{
    if (self.hudEndSEL) {
        [self performSelector: _hudEndSEL];
    }
    _isHudShow = NO;
}

#pragma mark -
#pragma mark - UITextView

- (void) textViewDidChange: (UITextView*) textView
{
    UCBREW_CALL_METHOD;
    [self layoutViews];
    [self resizeViews];
}

- (BOOL) textFieldShouldReturn: (UITextField*) textField
{
    if (textField.tag == kNFMessageComposerTagSubjectEditor) {
        [textField resignFirstResponder];
        [self.bodyEditor becomeFirstResponder];
        _bodyEditor.selectedRange = NSMakeRange (0, 0);
    }
    return NO;
}
#pragma mark -
#pragma mark - Keyboard

- (void) keyboardDidShow: (NSNotification*) notification
{
    CGRect keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    _keyboardHeight = keyboardRect.size.height > keyboardRect.size.width ? keyboardRect.size.width : keyboardRect
            .size.height;
    [self layoutViews];
    [self resizeViews];
}

- (void) keyboardWillHide: (NSNotification*) notification
{
    _keyboardHeight = 0;
    [self layoutViews];
    [self resizeViews];
}

#pragma mark -
#pragma mark TokenField

- (void) tokenFieldDidSelectAccessoryButton: (NFTokenField*) tokenField
{
    [self handleAttachmentAddButton: tokenField];
}

#pragma mark -
#pragma mark - AGImagePicker
- (void) agImagePickerController: (AGImagePickerController*) picker didFinishPickingMediaWithInfo: (NSArray*) info
{
    UCBREW_CALL_METHOD;
    [self.navigationController dismissModalViewControllerAnimated: YES];

    NSUInteger i = 1;
    [_attachmentEditor removeAllTokens];
    NSUInteger current_size = 0;
    self.attachments = [NSMutableArray arrayWithCapacity: 20];
    [self.navigationController dismissModalViewControllerAnimated: YES];
    for (ALAsset* asset in info) {
        if ([[asset valueForProperty: ALAssetPropertyType] isEqualToString: ALAssetTypePhoto]) {
            ALAssetRepresentation* representation = asset.defaultRepresentation;
            NSString* url;
            NSString* format;
            format = @"bmp";
            NSString* uti = [asset.defaultRepresentation UTI];
            if ([uti isEqualToString: @"public.jpeg"]) {
                format = @"jpeg";            
            } else if ([uti isEqualToString: @"public.png"]) {
                format = @"png";            
            } else if ([uti isEqualToString: @"public.gif"]) {
                format = @"gif";            
            } else if ([uti isEqualToString: @"public.tiff"]) {
                format = @"tiff";
            }
            url = [NSString stringWithFormat: @"%@", representation.url];
            if (current_size < 5 * 1024 * 1024) {
                long long asset_size = [[asset defaultRepresentation] size];
#if 0
                uint8_t* buf = (Byte*) malloc ((size_t)asset_size);
                NSUInteger buf_len = [representation getBytes: buf
                                                   fromOffset: 0 length: (NSUInteger) asset_size
                                                        error: nil];
                if (buf_len == 0) {
                    free (buf);
                    break;
                }

                NSData* asset_data = [NSData dataWithBytesNoCopy: buf length: buf_len freeWhenDone: YES];
#endif
                NSDictionary* asset_dict = [NSDictionary dictionaryWithObjectsAndKeys: format,
                                                                                       kNFMessageComposerAttachmentMimeType,
                                                                                        [NSNumber
                                                                                            numberWithLongLong: asset_size],
                                                                                        kNFMessageComposerAttachemntSize,
                                                                                        asset, kNFMessageComposerAttachementAsset,
                                                                                        nil];
                if (current_size + asset_size < 5 * 1024 * 1024) {
                    [_attachments addObject: asset_dict];
                    [_attachmentEditor addTokenWithTitle: [NSString stringWithFormat: @"ucsmth_%d.%@",
                                                                                      i, format] object: asset_dict];
                    i ++;
                    current_size += asset_size;
                }
                UCBREW_LOG(@"attachment size: %d", current_size);
            } else {
                break;
            }
        }
    }
    [self layoutViews];
}

- (void) agImagePickerController: (AGImagePickerController*) picker didFail: (NSError*) error
{
    [self.navigationController dismissModalViewControllerAnimated: YES];
}

@end
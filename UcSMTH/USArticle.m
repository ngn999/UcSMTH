//
//  USArticle.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-20.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USArticle.h"

@implementation USArticle
@synthesize authorId;
@synthesize date;
@synthesize content;
@synthesize contentHTML;
@synthesize reply;
@synthesize attachments;
@synthesize artcileId;

- (void) dealloc
{
    UCBREW_RELEASE (authorId);
    UCBREW_RELEASE (date);
    UCBREW_RELEASE (content);
    UCBREW_RELEASE (contentHTML);
    UCBREW_RELEASE (reply);
    UCBREW_RELEASE (attachments);
    UCBREW_RELEASE (artcileId);
    [super dealloc];
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"<USArticle: author: %@ date:%@ content: %@ reply: %@ attachments: %@>",
            authorId, date, content, reply, attachments];
}
@end

//
//  USBoardFeedParser.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "ParseKit.h"
#import "UcBrewKit.h"

#import "USBoardFeed.h"

#import "USBoardFeedParser.h"

static USBoardFeedParser* sharedParser = nil;

////////////////////////////////////////////////////////////////////////////////
static NSString* board_feed_grammer = @""
    @"@wordChars='.';"
    @"@start = boardHref;"
    @"boardHref ='/' 'board' '/' boardId;"
    @"boardId = (Word | Number)+"
    @"";

@interface USBoardFeedAssembly : NSObject
@property (nonatomic, retain) NSString* boardName;
@property (nonatomic, retain) NSString* boardId;

@property (nonatomic, retain) NSMutableArray* testarray;

- (void)parser: (PKParser*) parser didMatchBoardId: (PKAssembly*) assembly;
- (void)parser: (PKParser*) parser didMatchBoardHref: (PKAssembly*) assembly;
@end

@implementation USBoardFeedAssembly
@synthesize boardName;
@synthesize boardId;

@synthesize testarray;

- (id) init
{
    self = [super init];
    if (self) {
        testarray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void) dealloc
{
    UCBREW_RELEASE (boardName);
    UCBREW_RELEASE (boardId);
    [super dealloc];
}

- (void)parser: (PKParser*) parser didMatchBoardId: (PKAssembly*) assembly
{
    USBoardFeed* feed = nil;
    feed = assembly.target;
    if (feed == nil) {
        feed = [[[USBoardFeed alloc] init] autorelease];
        assembly.target = feed;
    }
    feed.boardId = [[assembly pop] stringValue];
}

- (void)parser: (PKParser*) parser didMatchBoardHref: (PKAssembly*) assembly
{
    UCBREW_ASSERT (assembly.target != nil);
}
@end
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
@interface USBoardFeedParser ()
@property (nonatomic, readwrite, retain) USBoardFeedAssembly* assembly;
@property (nonatomic, readwrite, retain) PKParser* parser;
@end
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
@implementation USBoardFeedParser
@synthesize assembly;
@synthesize parser;

+ (id) sharedParser
{
    @synchronized ([USBoardFeedParser class]) {
        if (sharedParser == nil) {
            sharedParser = [[USBoardFeedParser alloc] init];
        }
    }
    
    return sharedParser;
}

+ (id) allocWithZone:(NSZone *)zone
{
    @synchronized ([USBoardFeedParser class]) {
        if (sharedParser == nil) {
            sharedParser = [super allocWithZone: zone];
        }
    }
    return sharedParser;
}

- (id) init
{
    @synchronized ([USBoardFeedParser class]) {
        if ((self = [super init])) {
            self.assembly = [[[USBoardFeedAssembly alloc] init] autorelease];
            self.parser = [[PKParserFactory factory]
                           parserFromGrammar: board_feed_grammer
                           assembler: self.assembly];
        }
    }
    return self;
}

- (void) dealloc
{
    UCBREW_RELEASE (assembly);
    PKReleaseSubparserTree (self.parser);
    UCBREW_RELEASE (parser);
    [super dealloc];
}

- (NSUInteger) retainCount
{
    return NSUIntegerMax;
}

- (oneway void) release
{
    
}

- (id) retain
{
    return sharedParser;
}

- (id) parseBoardHref: (NSString*) href
{
    PKToken* t = nil;
    t = [self.parser  parse:href];
    return t;
}
@end
////////////////////////////////////////////////////////////////////////////////
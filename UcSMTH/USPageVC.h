//
// Created by kerberos on 12-8-23.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "USTableViewController.h"

@class USPageVC;
@protocol USPageVCDelegate <NSObject>
- (void) controller: (USPageVC*) controller didFinishPaging: (NSUInteger) page;
@end

@interface USPageVC : USTableViewController
@property (nonatomic, assign) id<USPageVCDelegate> delegate;
@property (nonatomic, readonly) NSUInteger indexOfPage;
@property (nonatomic, readonly) NSUInteger totalOfPage;

+ (id) viewControllerWithIndexOfPage: (NSUInteger) index
                         totalOfPage: (NSUInteger) total
                            delegate: (id) delegate;
@end
//
// Created by kerberos on 12-8-26.
//
// To change the template use AppCode | Preferences | File Templates.
//

#import "UcBrewKit.h"
#import "NFTokenFeild.h"

#define kTokenFieldFontSize 14.0
#define kTokenFieldPaddingX 6.0
#define kTokenFieldPaddingY 6.0
#define kTokenFieldTokenHeight (kTokenFieldFontSize + 4.0)
#define kTokenFieldMaxTokenWidth 260.0
#define kTokenFieldFrameKeyPath @"frame"
#define kTokenFieldShadowHeight 14.0

@interface NFToken()
@end

@implementation NFToken
{
@private
    NSString* _title;
    id _userInfo;
}
@synthesize title = _title;
@synthesize userInfo = _userInfo;

+ (id) tokenWithTitle: (NSString*) title userInfo: (id) userInfo
{
    NFToken* token = [self buttonWithType: UIButtonTypeCustom];
    token.userInfo = userInfo;
    token.title = title;
    token.backgroundColor = [UIColor clearColor];
    UIFont* font = [UIFont systemFontOfSize: kTokenFieldFontSize];
    CGSize token_size = [title sizeWithFont: font];
    token_size.width = MIN(kTokenFieldMaxTokenWidth, token_size.width);
    token_size.width += kTokenFieldPaddingX * 2.0f;

    token_size.height = MIN(kTokenFieldFontSize, token_size.height);
    token_size.height += kTokenFieldPaddingY * 2.0f;

    token.frame = (CGRect) {CGPointZero, token_size};
    token.titleLabel.font = font;
    token.showsTouchWhenHighlighted = YES;
    return token;
}

- (void) drawRect: (CGRect) rect
{
    UCBREW_CALL_METHOD;
    CGFloat radius = CGRectGetHeight (self.bounds) / 2.0f;
    UIBezierPath* path = [UIBezierPath bezierPathWithRoundedRect: self.bounds
                                                    cornerRadius: radius];
    CGContextRef ctx = UIGraphicsGetCurrentContext ();
    CGContextSaveGState (ctx);
    CGContextAddPath (ctx, path.CGPath);
    CGContextClip (ctx);

    CGColorRef colors[2];
    if (self.highlighted) {
        colors[0] = [UIColor colorWithRed: 0.322 green: 0.541 blue: 0.976 alpha: 1.0].CGColor;
        colors[1] = [UIColor colorWithRed: 0.235 green: 0.329 blue: 0.973 alpha: 1.0].CGColor;
    } else {
        colors[0] =  [UIColor colorWithRed:0.863 green:0.902 blue:0.969 alpha:1.0].CGColor;
        colors[1] =  [UIColor colorWithRed:0.741 green:0.808 blue:0.937 alpha:1.0].CGColor;
    }
    CFArrayRef colors_ref = CFArrayCreate (kCFAllocatorDefault, (const void **)colors, (CFIndex) 2, &kCFTypeArrayCallBacks);

    CGColorSpaceRef color_space = CGColorSpaceCreateDeviceRGB ();
    CGGradientRef gradient = CGGradientCreateWithColors (color_space, colors_ref, NULL);

    CGColorSpaceRelease (color_space);
    CGContextDrawLinearGradient (ctx, gradient, CGPointZero, CGPointMake (0.0f, CGRectGetHeight (self.bounds)), 0);

    CGGradientRelease (gradient);
    CFRelease (colors_ref);
    CGContextRestoreGState (ctx);


    if (self.highlighted) {
        [[UIColor colorWithRed:0.275 green:0.478 blue:0.871 alpha:1.0] set];
    }
    else {
        [[UIColor colorWithRed:0.667 green:0.757 blue:0.914 alpha:1.0] set];
    }

    path = [UIBezierPath bezierPathWithRoundedRect:CGRectInset(self.bounds, 0.5, 0.5) cornerRadius:radius];
    [path setLineWidth:1.0];
    [path stroke];

    if (self.highlighted) {
        [[UIColor whiteColor] set];
    }
    else {
        [[UIColor blackColor] set];
    }

    UIFont * title_font = [UIFont systemFontOfSize:kTokenFieldFontSize];
    CGSize title_size = [self.title sizeWithFont: title_font];
    CGRect title_frame = CGRectMake((CGRectGetWidth(self.bounds) - title_size.width) / 2.0,
        (CGRectGetHeight(self.bounds) - title_size.height) / 2.0,
        title_size.width,
        title_size.height);

    [self.title drawInRect: title_frame withFont: title_font lineBreakMode: UILineBreakModeTailTruncation
                 alignment: UITextAlignmentCenter];
}

- (void) dealloc
{
    [_title release], _title = nil;
    [_userInfo release], _userInfo = nil;
    [super dealloc];
}
@end

@interface NFTokenField () <UITextFieldDelegate>
@property (nonatomic, readwrite) CGSize contentSize;

@end

@implementation NFTokenField
{
@private
    id <NFTokenFieldDelegate> _delegate;
    UIView* _accessoryView;
    NFTokenFieldAccessoryType _accessoryType;
    NSMutableArray* _tokens;
    CGSize _contentSize;
    UILabel* _textLabel;
}

@synthesize delegate = _delegate;
@synthesize accessoryView = _accessoryView;
@synthesize textLabel = _textLabel;


- (NSArray*) tokens
{
    return _tokens;
}

- (NFTokenFieldAccessoryType) accessoryType
{
    return _accessoryType;
}

- (void) setAccessoryType: (NFTokenFieldAccessoryType) accessoryType
{
    if (_accessoryType != accessoryType) {
        if (_accessoryView) {
            [self.accessoryView removeFromSuperview];
            self.accessoryView = nil;
        }

        if (accessoryType == NFTokenFeildAccessoryTypeAdd) {
            _accessoryView = [[UIButton buttonWithType: UIButtonTypeContactAdd] retain];
            [(UIButton*)_accessoryView addTarget: self action: @selector(handleAccessoryTouched:)
                                forControlEvents: UIControlEventTouchUpInside];
            [_accessoryView sizeToFit];
            [self addSubview: _accessoryView];
        }
        _accessoryType = accessoryType;
        [self setNeedsLayout];
    }
}

- (CGSize) contentSize
{
    NSUInteger row = 0;
    NSUInteger token_count = self.tokens.count;
    CGFloat left = kTokenFieldPaddingX;
    CGFloat max_left = CGRectGetWidth (self.bounds) - kTokenFieldPaddingX;
    CGFloat row_height = [self rowHeight];

    CGRect work_frame = CGRectZero;
    [_textLabel sizeToFit];
    if (_textLabel) {
        work_frame = _textLabel.frame;
        work_frame.origin.x = left;
        CGFloat label_y = (row_height - CGRectGetHeight (work_frame))/2;
        if (label_y < 0) {
            label_y = 0;
        }
        work_frame.origin.y = label_y + kTokenFieldPaddingY;
        left += work_frame.size.width;
        left += kTokenFieldPaddingX;
    }

    for (int i = 0; i < token_count; i++) {
        NFToken* t = [_tokens objectAtIndex: i];
        CGFloat right = left + CGRectGetWidth (t.bounds);
        if (right > max_left) {
            row ++;
            left = kTokenFieldPaddingX;
        }

        work_frame = t.frame;
        work_frame.origin = CGPointMake (left, (CGFloat)row * row_height + (row_height - CGRectGetHeight (
            work_frame)) / 2.0 + kTokenFieldPaddingY);

        left += CGRectGetWidth (work_frame) + kTokenFieldPaddingX;
    }

    if (_accessoryView) {
        CGFloat max_left_with_button = max_left - kTokenFieldPaddingX - CGRectGetWidth (_accessoryView.frame);
        if (max_left_with_button - left < 0) {
            row ++;
            left = kTokenFieldPaddingX;
        }
        work_frame = _accessoryView.frame;
        work_frame.origin = CGPointMake (max_left_with_button, (CGFloat)row * row_height + (row_height - CGRectGetHeight (
            work_frame)) / 2.0 + kTokenFieldPaddingY);
    }
    return CGSizeMake (self.frame.size.width, row * row_height + kTokenFieldTokenHeight * 2);
}

- (id) initWithFrame: (CGRect) frame
{
    UCBREW_CALL_METHOD;
    self = [super initWithFrame: frame];
    if (self) {
        self.clipsToBounds = YES;
        _tokens = [[NSMutableArray alloc] init];
        _accessoryType = NFTokenFieldAccessoryTypeNone;
        if (_accessoryType == NFTokenFeildAccessoryTypeAdd) {
            UIButton* button = [UIButton buttonWithType: UIButtonTypeContactAdd];
            [button addTarget: self action: @selector(handleAccessoryTouched:)
             forControlEvents: UIControlEventTouchUpInside];
            _accessoryView = [button retain];
            [self addSubview: button];
        }
        _textLabel = [[UILabel alloc] initWithFrame: CGRectZero];
        [self addSubview: _textLabel];
        [self setNeedsLayout];
    }
    return self;
}

- (void) dealloc
{
    UCBREW_CALL_METHOD;
    [_accessoryView release], _accessoryView = nil;
    [_tokens release], _tokens = nil;
    [_textLabel release], _textLabel = nil;
    [super dealloc];
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    UCBREW_CALL_METHOD;
    NSUInteger row = 0;
    NSUInteger token_count = self.tokens.count;
    CGFloat left = kTokenFieldPaddingX;
    CGFloat max_left = CGRectGetWidth (self.bounds) - kTokenFieldPaddingX;
    CGFloat row_height = [self rowHeight];

    CGRect work_frame = CGRectZero;

    if (_textLabel) {
        work_frame = _textLabel.frame;
        work_frame.origin.x = left;
        CGFloat label_y = (row_height - CGRectGetHeight (work_frame))/2;
        if (label_y < 0) {
            label_y = 0;
        }
        work_frame.origin.y = label_y + kTokenFieldPaddingY;
        self.textLabel.frame = work_frame;
        left += work_frame.size.width;
        left += kTokenFieldPaddingX;
    }

    for (int i = 0; i < token_count; i++) {
        NFToken* t = [_tokens objectAtIndex: i];
        CGFloat right = left + CGRectGetWidth (t.bounds);
        if (right > max_left) {
            row ++;
            left = kTokenFieldPaddingX;
        }

        work_frame = t.frame;
        work_frame.origin = CGPointMake (left, (CGFloat)row * row_height + (row_height - CGRectGetHeight (
            work_frame)) / 2.0 + kTokenFieldPaddingY);
        t.frame = work_frame;

        left += CGRectGetWidth (work_frame) + kTokenFieldPaddingX;
    }

    if (_accessoryView) {
        CGFloat max_left_with_button = max_left - kTokenFieldPaddingX - CGRectGetWidth (_accessoryView.frame);
        if (max_left_with_button - left < 0) {
            row ++;
            left = kTokenFieldPaddingX;
        }
        work_frame = _accessoryView.frame;
        work_frame.origin = CGPointMake (max_left_with_button, (CGFloat)row * row_height + (row_height - CGRectGetHeight (
            work_frame)) / 2.0 + kTokenFieldPaddingY);
        _accessoryView.frame = work_frame;
    }
}

#pragma mark -
#pragma mark Private

- (CGFloat) rowHeight
{
    CGFloat button_height = CGRectGetHeight(self.accessoryView.frame);
    return MAX(button_height, (kTokenFieldPaddingY * 2.0f + kTokenFieldTokenHeight));
}

#pragma mark -
#pragma mark Actions

- (void) handleAccessoryTouched: (id) sender
{
    NSLog (@"AccessoryTouched");
    if (self.delegate && [self.delegate respondsToSelector: @selector(tokenFieldDidSelectAccessoryButton:)]) {
        [self.delegate performSelector: @selector(tokenFieldDidSelectAccessoryButton:) withObject: self];
    }
}

- (void) handleTokenTouched: (id) sender
{
    if (self.delegate && [self.delegate respondsToSelector: @selector(tokenField:didSelectToken:)]) {
        [self.delegate performSelector: @selector(tokenField:didSelectToken:) withObject: self withObject:
            sender];
    }
}

- (void) addTokenWithTitle: (NSString*) title object: (id) object
{
    NFToken* token = [NFToken tokenWithTitle: title userInfo: object];
    [_tokens addObject: token];
    [token addTarget: self action: @selector(handleTokenTouched:) forControlEvents: UIControlEventTouchUpInside];
    [self addSubview: token];
    [self setNeedsLayout];
}

- (void) removeToken: (NFToken*) token
{
    [_tokens removeObject: token];
    [token removeFromSuperview];
    [self setNeedsLayout];
}

- (void) removeTokenByObject: (id) object
{
    for (NFToken* iter in [NSArray arrayWithArray: _tokens]) {
        if (iter.userInfo == object) {
            [_tokens removeObject: iter];
            [iter removeFromSuperview];
        }
    }
    [self setNeedsLayout];
}

- (void) removeAllTokens
{
    for (NFToken*  iter in _tokens) {
        [iter removeFromSuperview];
    }

    [_tokens removeAllObjects];
    [self setNeedsLayout];
}


@end

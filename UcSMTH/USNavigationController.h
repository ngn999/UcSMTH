//
//  USNavigationController.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UcBrewKit.h"

@interface USNavigationController : UINavigationController

@end

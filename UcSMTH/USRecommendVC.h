//
//  USRecommendVC.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-19.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USTableViewController.h"

@interface USRecommendVC : USTableViewController
+ (id) viewController;
@end

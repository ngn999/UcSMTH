//
//  USMailWebModel.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USHtmlRequestModel.h"

@interface USMailWebModel : USHtmlRequestModel
@property (nonatomic, readonly, copy) NSString* mailSubject;
@property (nonatomic, readonly, copy) NSString* boxId;
@property (nonatomic, readonly, copy) NSString* mailId;
@property (nonatomic, readonly, copy) NSString* mailDate;
@property (nonatomic, readonly, copy) NSString* mailAuthor;
@property (nonatomic, readonly, copy) NSString* mailBody;

+ (id) modelWithBoxId: (NSString*) aBoxId
              mailId: (NSString*) aMailId;
@end

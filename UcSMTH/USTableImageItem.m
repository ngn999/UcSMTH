//
//  USTableImageItem.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-21.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USTableImageItemCell.h"
#import "USTableImageItem.h"

@implementation USTableImageItem

- (Class) cellClass
{
    return [USTableImageItemCell class];
}
@end

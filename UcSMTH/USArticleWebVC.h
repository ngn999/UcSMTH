//
//  USArticleWebVC.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-21.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcSMTHConfig.h"
#import "TTModelViewController.h"
#import "USPageVC.h"

@interface USArticleWebVC : TTModelViewController <UIWebViewDelegate, USPageVCDelegate>
+ (id) viewControllerWithBoardId: (NSString*) aBoardId articleId: (NSString*) aArticleId;
+ (id) viewControllerWithBoardId: (NSString*) aBoardId articleId: (NSString*) aArticleId viewMode: (NSUInteger) aViewMode;
@end

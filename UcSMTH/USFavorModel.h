//
//  USFavorModel.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USHtmlRequestModel.h"
#import "USURLRequestDelegate.h"

@interface USFavorModel : USHtmlRequestModel <USURLRequestDelegate>
@property (nonatomic, copy) NSString* dirName;
@property (nonatomic, copy) NSString* dirId;
@property (nonatomic, retain) NSArray* boardFeeds;
- (id) initWithDirName: (NSString*) dirName dirId: (NSString*) dirId;
@end

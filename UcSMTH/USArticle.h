//
//  USArticle.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-20.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface USArticle : NSObject
@property (nonatomic, copy) NSString* authorId;
@property (nonatomic, copy) NSString* date;
@property (nonatomic, copy) NSString* content;
@property (nonatomic, copy) NSString* reply;
@property (nonatomic, copy) NSString* contentHTML;
@property (nonatomic, copy) NSString* artcileId;
@property (nonatomic, retain) NSArray* attachments;
@end

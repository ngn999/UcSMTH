//
//  USURLRequest.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "TTURLRequest.h"

@interface USURLRequest : TTURLRequest

+ (id) requestWithURL: (NSString*) url delegate: (id) delegate;
@end

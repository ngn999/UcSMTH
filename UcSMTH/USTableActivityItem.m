//
//  USTableActivityItem.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-27.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USTableActivityItemCell.h"
#import "USTableActivityItem.h"

@implementation USTableActivityItem
@synthesize text;
@synthesize activityIndicatorView;

- (NSString*) reuseIdentifier
{
    return NSStringFromClass([USTableActivityItemCell class]);
}

- (void) dealloc
{
    UCBREW_RELEASE(activityIndicatorView);
    UCBREW_RELEASE(text);
    [super dealloc];
}
@end

//
//  USTableSwitchCell.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-27.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USTableSwitchItem.h"
#import "USTableSwitchItemCell.h"

@implementation USTableSwitchItemCell
@synthesize switchView;

- (void) dealloc
{
    UCBREW_RELEASE(switchView);
    [super dealloc];
}

- (void) setItem:(USTableSwitchItem*)item
{
    [super setItem: item];
    self.textLabel.text = item.text;
    self.switchView = item.switchView;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    CGRect r = self.contentView.bounds;
    [self.switchView sizeToFit];
    CGRect rs = self.switchView.frame;
    rs.origin.y = (r.size.height - rs.size.height) / 2;
    rs.origin.x = (r.size.width - rs.size.width - 8);
    self.switchView.frame = rs;
    CGRect rl = self.textLabel.frame;
    [self.switchView removeFromSuperview];
    [self.contentView addSubview: self.switchView];
    
    rl.origin.x = 8;
    rl.origin.y = 0;
    rl.size.width = rs.origin.x - rl.origin.x - 4;
    self.textLabel.frame = rl;
}

@end

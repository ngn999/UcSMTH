//
//  USWebController.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-24.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USNavigationController.h"
#import "USWebController.h"
#import "USAppDelegate.h"

@interface USWebController ()
@end

static USWebController* sharedController = nil;

@implementation USWebController
+ (void) presentSharedModelControllerWithURL:(NSString *)url
{
    @synchronized ([USWebController class]) {
        if (sharedController == nil) {
            sharedController = [[USWebController alloc] init];
        }
        [sharedController openURL: [NSURL URLWithString: url]];
    }

    USNavigationController* nav = [[USNavigationController alloc] initWithRootViewController: sharedController];
    USAppDelegate* appDelegate = nil;
    appDelegate = (USAppDelegate*) [[UIApplication sharedApplication] delegate];
    [appDelegate.viewController presentModalViewController: nav animated: YES];
}

- (id) init
{
    @synchronized ([USWebController class]) {
        self = [super init];
        if (self) {
        }
    }
    return self;
}

- (oneway void) release
{
    
}

- (NSUInteger) retainCount
{
    return NSUIntegerMax;
}

- (id) autorelease
{
    return sharedController;
}

- (void) handleCloseAction
{
    [self dismissModalViewControllerAnimated: YES];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem* doneItem = [[[UIBarButtonItem alloc] initWithTitle: NSLocalizedString(@"close", nil)
                                                                  style: UIBarButtonItemStyleBordered
                                                                 target: self
                                                                 action: @selector(handleCloseAction)]
                                 autorelease];
    self.navigationItem.leftBarButtonItem = doneItem;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

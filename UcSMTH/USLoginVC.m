//
//  USSignUpVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-27.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "SSKeychain.h"
#import "UcSMTHEngine.h"
#import "USURLRequest.h"
#import "USURLRequestDelegate.h"
#import "USURLHtmlResponse.h"
#import "USTableTextFieldItem.h"
#import "USTableTextFieldItemCell.h"
#import "USTableSwitchItemCell.h"
#import "USTableActivityItem.h"
#import "USTableSwitchItem.h"
#import "USLoginVC.h"

@interface USLoginVC () <USURLRequestDelegate, UITextFieldDelegate>
@property (nonatomic, assign) id<USLoginVCDelegate> delegate;
@property (nonatomic, retain) UITextField* usernameField;
@property (nonatomic, retain) UITextField* passwordField;
@property (nonatomic, retain) UISwitch* autoSignUpSwitch;
@property (nonatomic, retain) UIActivityIndicatorView* loadingView;
@property (nonatomic, copy) NSString* errorMessage;
@property (nonatomic, retain) USURLRequest* loginRequest;
@end

@implementation USLoginVC
@synthesize usernameField;
@synthesize passwordField;
@synthesize autoSignUpSwitch;
@synthesize loadingView;
@synthesize errorMessage;
@synthesize loginRequest;
@synthesize delegate;

+ (id) viewControllerWithDelegate:(id)delegate
{
    USLoginVC* vc = [[USLoginVC alloc] initWithStyle: UITableViewStyleGrouped delegate: delegate];
    return [vc autorelease];
}

- (id) initWithStyle:(UITableViewStyle)style delegate: (id) aDelegate
{
    self = [super initWithStyle: style];
    if (self) {
        self.delegate = aDelegate;
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView
{
    [super loadView];
    self.view.backgroundColor = [UIColor colorWithRed: 0.917f
                                                green: 0.917f
                                                 blue: 0.917f
                                                alpha: 1.0f];
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    self.usernameField = [[[UITextField alloc] initWithFrame: CGRectZero] autorelease];
    self.usernameField.placeholder = NSLocalizedString(@"Please input username.", nil);
    self.usernameField.keyboardType = UIKeyboardTypeAlphabet;
    self.passwordField = [[[UITextField alloc] initWithFrame: CGRectZero] autorelease];
    self.passwordField.placeholder = NSLocalizedString(@"Please input password.", nil);
    self.passwordField.secureTextEntry = YES;
    self.passwordField.keyboardType = UIKeyboardTypeAlphabet;
    self.autoSignUpSwitch = [[[UISwitch alloc] initWithFrame: CGRectZero] autorelease];
    self.autoSignUpSwitch.on = [[engine userDefaults] boolForKey: @"autologin"];
    self.loadingView = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleGray] autorelease];
    [self.autoSignUpSwitch addTarget: self action: @selector(handleSwitchAction:) forControlEvents:UIControlEventValueChanged];
    
    if (self.autoSignUpSwitch.on) {
        NSString* username = [[engine userDefaults] stringForKey: @"username"];
        self.usernameField.text = username;
        NSString* password = [SSKeychain passwordForService: [engine bundleIdentifier] account: username];
        self.passwordField.text = password;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setupUI];
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemCancel
                                                                                           target:self
                                                                                           action: @selector(handleCancelAction)]
                                             autorelease];
    if (self.autoSignUpSwitch.on == YES
        && UcBrew_IsNotEmptyString (self.usernameField.text)
        && UcBrew_IsNotEmptyString(self.passwordField.text)) {
        [self handleLoginAction];
    }
}

- (void) setupUI
{
    MwfTableData* table_data = [MwfTableData createTableDataWithSections];
    [table_data addSection:@""];
    USTableTextFieldItem* ti = nil;
    ti = [[[USTableTextFieldItem alloc] init] autorelease];
    ti.text = NSLocalizedString(@"Username", nil);
    ti.textField = self.usernameField;
    [table_data addRow: ti inSection: 0];
    
    ti = [[[USTableTextFieldItem alloc] init] autorelease];
    ti.text = NSLocalizedString(@"Password", nil);
    ti.textField = self.passwordField;
    [table_data addRow: ti inSection: 0];
    
    
    USTableSwitchItem* si = [[[USTableSwitchItem alloc] init] autorelease];
    si.text = NSLocalizedString(@"Auto Login", nil);
    si.switchView = self.autoSignUpSwitch;
    
    [table_data addRow: si inSection: 0];
    [table_data addSection:@""];
    USTableActivityItem* ai = [[[USTableActivityItem alloc] init] autorelease];
    ai.text = @"login";
    ai.activityIndicatorView = self.loadingView;
    [table_data addRow: ai inSection:1];
    self.tableData = table_data;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    UCBREW_RELEASE(loadingView);
    UCBREW_RELEASE(usernameField);
    UCBREW_RELEASE(passwordField);
    UCBREW_RELEASE(autoSignUpSwitch);
}

- (void) dealloc
{
    UCBREW_RELEASE(loadingView);
    if (loginRequest.isLoading) {
        [loginRequest cancel];
    }
    UCBREW_RELEASE(loginRequest);
    UCBREW_RELEASE(usernameField);
    UCBREW_RELEASE(passwordField);
    UCBREW_RELEASE(autoSignUpSwitch);
    [super dealloc];
}

- (NSString*) title
{
    return NSLocalizedString(@"Login", nil);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (MwfTableData*) createAndInitTableData
{
    return nil;
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}


- $cellFor (UITextField)
{
    USTableTextFieldItemCell* cell = nil;
    static NSString* identifier = @"TextFieldCell";
    cell = [self.tableView dequeueReusableCellWithIdentifier: identifier];
    if (cell == nil) {
        cell = [[USTableTextFieldItemCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: identifier];
    }
    return cell;
}

- $cellFor (UISwitch)
{
    USTableSwitchItemCell* cell = nil;
    static NSString* identifier = @"SwitchCell";
    cell = [self.tableView dequeueReusableCellWithIdentifier: identifier];
    if (cell == nil) {
        cell = [[USTableSwitchItemCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: identifier];
    }
    return cell;
}

- $configCell (UISwitch, USTableSwitchItemCell)
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.switchView = item;
}

- $configCell (UITextField, USTableTextFieldItemCell)
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textField = item;
}

- (void) handleSwitchAction: (UISwitch*) switchControl
{
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    if (switchControl == self.autoSignUpSwitch) {
        [[engine userDefaults] setBool: switchControl.on forKey: @"autologin"];
        if (switchControl.on == NO) {
            [self removeAllPassword];
        }
    }
}

- (void) handleCancelAction
{
    if (self.loginRequest.isLoading) {
        [self.loginRequest cancel];
    } else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(loginDidCancel:)]) {
            [self.delegate performSelector: @selector(loginDidCancel:) withObject: self];
        }
    }
}

- (void) handleLoginAction
{
    if (self.loginRequest.isLoading) {
        [self.loginRequest cancel];
        return;
    } else {
        NSString* login_url = @"http://m.newsmth.net/user/login";
        NSString* username = self.usernameField.text;
        NSString* password = self.passwordField.text;
        self.loginRequest = [USURLRequest requestWithURL: login_url delegate: self];
        self.loginRequest.httpMethod = @"POST";
        self.loginRequest.cachePolicy = TTURLRequestCachePolicyNetwork;
        [self.loginRequest.parameters setObject: username forKey: @"id"];
        [self.loginRequest.parameters setObject: password forKey: @"passwd"];
        [self.loginRequest.parameters setObject: @"on" forKey: @"save"];
        USURLHtmlResponse * resp = [USURLHtmlResponse response];
        self.loginRequest.response = resp;
        [self.loginRequest send];
    }
}

- (void) requestDidStartLoad:(TTURLRequest *)request
{
    /*
     * activity start and show
     */
    [self.loadingView startAnimating];
}

- (void) requestDidCancelLoad:(TTURLRequest *)request
{
    self.loginRequest = nil;
    /*
     * stop activity
     */
    [self.loadingView stopAnimating];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(loginDidCancel:)]) {
        [self.delegate performSelector: @selector(loginDidCancel:) withObject: self];
    }
}

- (void) request:(TTURLRequest *)request didFailLoadWithError:(NSError *)error
{
    [self.loadingView stopAnimating];
    self.loginRequest = nil;
    /*
     * show error
     */
    UCBREW_LOG_OBJECT([error localizedDescription]);
}

- (void) storePassword: (NSString*) password forUser: (NSString*) username
{
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    [SSKeychain setPassword: password forService: [engine bundleIdentifier] account: username];
    [engine.userDefaults setValue: username forKey: @"username"];
}

- (void) removeAllPassword
{
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    [engine.userDefaults setNilValueForKey: @"username"];
    NSArray* users = [SSKeychain accountsForService: [engine bundleIdentifier]];
    for (NSString* s in users) {
        [SSKeychain deletePasswordForService: [engine bundleIdentifier] account: s];
    }
}
                                                      
- (void) requestDidFinishLoad:(TTURLRequest *)request
{
    USURLHtmlResponse * resp = UCBREW_DYNAMIC_CAST(request.response, USURLHtmlResponse);
    DDXMLDocument* doc = resp.htmlDocument;
    static NSString* error_xpath = @"//body/div[@id='wraper']/div[@id='m_main']/div[@class='sp hl f'][1]";
    NSArray* nodes = [doc nodesForXPath: error_xpath error:nil];
    @try {
        NSString* ret_title = [[nodes objectAtIndex: 0] stringValue];
        NSRange r = [ret_title rangeOfString:@"成功"];
        if (!r.length) {
            /*
             * error
             */
            NSError* error = [[[NSError alloc] initWithDomain: UCBREW_DOMAIN code: 101 userInfo: [NSDictionary dictionaryWithObject: ret_title forKey: NSLocalizedDescriptionKey]] autorelease];
            [self request: request didFailLoadWithError: error];
            return;
        }
    }
    @catch (NSException *exception) {
         
    }
    [self.loadingView stopAnimating];
    self.loginRequest = nil;
    /*
     * login success ?
     */
    if (self.autoSignUpSwitch.on) {
        [self storePassword: self.passwordField.text forUser: self.usernameField.text];
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(loginDidFinish:)]) {
        [self.delegate performSelector: @selector(loginDidFinish:) withObject: self];
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        [self handleLoginAction];
    }
}
@end
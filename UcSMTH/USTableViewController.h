//
//  USTableViewController.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//
#import "UcBrewKit.h"
#import "USLoginVC.h"
#import "TTTableViewController.h"

@interface USTableViewController : TTTableViewController <USLoginVCDelegate>
- (void) loginDidFinish:(USLoginVC*)controller;
- (void) loginDidCancel:(USLoginVC*)controller;
- (void) presentLoginVCWithAnimated: (BOOL) animated;
@end

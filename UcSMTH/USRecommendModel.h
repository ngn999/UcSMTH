//
//  USRecomModel.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "TTModel.h"

@interface USRecommendModel : TTModel
@property (nonatomic, retain, readonly) NSMutableDictionary* recommendFeeds;
@end
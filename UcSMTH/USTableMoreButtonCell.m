//
// Created by kerberos on 12-8-22.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "USTableMoreButtonCell.h"
#import "UcBrewKit.h"


@implementation USTableMoreButtonCell
{

}
- (void) setObject: (id) object
{
    if ( _item != object ){
        [super setObject: object];
        USTableMoreButtonCell* item = (USTableMoreButtonCell*) object;
        self.accessoryType = _item.accessoryType;
    }
}
@end
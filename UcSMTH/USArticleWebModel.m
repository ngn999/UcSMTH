//
//  USArticleWebModel.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-22.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "UcSMTHEngine.h"
#import "UcSMTHConfig.h"
#import "USArticle.h"
#import "USURLHtmlResponse.h"
#import "USURLRequest.h"
#import "USArticleWebModel.h"
#import "RegexKitLite.h"

@interface USArticleWebModel ()
@property (nonatomic, readwrite, copy) NSString* articleSubject;
@property (nonatomic, readwrite, copy) NSString* boardId;
@property (nonatomic, readwrite, copy) NSString* articleId;
@property (nonatomic, readwrite, assign) NSUInteger viewMode;
@property (nonatomic, readwrite, retain) NSArray* articles;

@property (nonatomic, readwrite, assign) NSUInteger indexOfPage;
@property (nonatomic, readwrite, assign) NSUInteger totalOfPage;
@property (nonatomic, assign) NSUInteger startIndexOfPage;
@property (nonatomic, assign) BOOL showImage;
@end

@implementation USArticleWebModel
@synthesize articleSubject;
@synthesize articleId;
@synthesize boardId;
@synthesize viewMode;
@synthesize indexOfPage;
@synthesize totalOfPage;
@synthesize startIndexOfPage;
@synthesize articles;
@synthesize showImage;

+ (id) modelWithBoardId: (NSString *)aBoardId
              articleId: (NSString *)aArticleId
       startIndexOfPage: (NSUInteger) aStartIndexOfPage
               viewMode:(NSUInteger)aViewMode
{
    USArticleWebModel* model = [[USArticleWebModel alloc] initWithBoardId: aBoardId
                                                                articleId: aArticleId
                                                         startIndexOfPage: aStartIndexOfPage
                                                                 viewMode: aViewMode];
    return [model autorelease];
}

+ (id) modelWithBoardId:(NSString *)aBoardId articleId:(NSString *)aArticleId viewMode:(NSUInteger)aViewMode
{
    USArticleWebModel* model = [[USArticleWebModel alloc] initWithBoardId: aBoardId articleId: aArticleId viewMode: aViewMode];
    return [model autorelease];
}

+ (id) modelWithBoardId:(NSString *)aBoardId articleId:(NSString *)aArticleId
{
    USArticleWebModel* model = [[USArticleWebModel alloc] initWithBoardId: aBoardId articleId: aArticleId];
    return [model autorelease];
}

- (id) initWithBoardId: (NSString*) aBoardId articleId: (NSString*) aArticleId
{
    self = [self initWithBoardId: aBoardId articleId: aArticleId viewMode: 2];
    return self;
}

- (id) initWithBoardId: (NSString*) aBoardId articleId: (NSString*) aArticleId viewMode: (NSUInteger) aViewMode
{
    self = [self initWithBoardId: aBoardId articleId: aArticleId startIndexOfPage: 1 viewMode: aViewMode];
    return self;
}

- (id) initWithBoardId: (NSString*) aBoardId
             articleId: (NSString*) aArticleId
      startIndexOfPage: (NSUInteger) aStartIndexOfPage
              viewMode: (NSUInteger) aViewMode
{
    self = [super init];
    if (self) {
        self.boardId = aBoardId;
        self.articleId = aArticleId;
        self.viewMode = aViewMode;
        self.startIndexOfPage = aStartIndexOfPage;
        self.showImage = [[[UcSMTHEngine sharedEngine] userDefaults] boolForKey: @"showimage"];
    }
    
    return self;
}

- (void) dealloc
{
    UCBREW_CALL_METHOD;
    UCBREW_RELEASE (articles);
    UCBREW_RELEASE (boardId);
    UCBREW_RELEASE (articleId)
    UCBREW_RELEASE (articleSubject);
    [super dealloc];
}

- (void) load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
    if (more) {
        if (indexOfPage < totalOfPage) {
            indexOfPage ++;
        } else{
            return;
        }
    } else {
        indexOfPage = self.startIndexOfPage;
        totalOfPage = 0;
    }
    NSString* post_url = nil;
    if (self.viewMode == 2) {
        post_url = [NSString stringWithFormat: @"http://m.newsmth.net/article/%@/%@?p=%d",
                          self.boardId, self.articleId, self.indexOfPage];
    } else if (self.viewMode == 0){
        post_url = [NSString stringWithFormat: @"http://m.newsmth.net/article/%@/single/%@/0?p=%d",
                          self.boardId, self.articleId, self.indexOfPage];
    }
    //NSString* post_url = [NSString stringWithFormat: @"http://m.newsmth.net/article/%@/%@?p=%d",
    //                      self.boardId, self.articleId, self.indexOfPage];
    //post_url = @"http://m.newsmth.net/article/Diablo/70525";
    USURLRequest* request = [USURLRequest requestWithURL: post_url
                                                delegate: self];
    request.cachePolicy = TTURLRequestCachePolicyNetwork;
    USURLHtmlResponse * resp = [USURLHtmlResponse response];
    request.response = resp;
    [request send];
}

- (void) requestDidStartLoad:(TTURLRequest *)request
{
    [super requestDidStartLoad: request];
}

- (void) requestDidFinishLoad:(TTURLRequest *)request
{
    USURLHtmlResponse* resp = UCBREW_DYNAMIC_CAST(request.response, USURLHtmlResponse);
    if (resp) {
        [self processHtmlDocument: resp.htmlDocument];
    }
    [super requestDidFinishLoad: request];
    //[self didFinishLoad];
}

- (void) processHtmlDocument: (DDXMLDocument*) doc
{
    NSError* error = nil;
    static NSString* sec_nav_xpath = @"//div[@id='m_main']/div[@class='sec nav'][2]";
    static NSString* article_nodes_xpath = @"//div[@id='m_main']/ul[@class='list sec']/li";
    static NSString* article_subject_xpath = @"//div[@id='m_main']/ul[@class='list sec']/li[@class='f'][1]";
    
    NSArray* nodes = nil;
    NSUInteger total_page = 0;
    
    nodes = [doc nodesForXPath: sec_nav_xpath error: &error];
    if (error) {
        UCBREW_LOG(@"Error: %@\n\n", [error localizedDescription]);
    } else {
        if (!!nodes.count) {
            NSString* total_page_string = [[nodes objectAtIndex: 0] stringValue];
            /*
             * TODO: best parsing
             */
            PKTokenizer* t = [PKTokenizer tokenizerWithString: total_page_string];
            PKToken* eof = [PKToken EOFToken];
            PKToken* tok = nil;
            NSInteger num_index = 0;
            while ( (tok = [t nextToken]) != eof) {
                if (tok.isNumber) {
                    num_index ++;
                    if (num_index == 2) {
                        total_page = (NSInteger)[tok floatValue];
                        break;
                    }
                }
            }
        }
    }
    
    self.totalOfPage = total_page;
    
    NSString* article_subject = nil;
    @try {
        article_subject = [[[doc nodesForXPath: article_subject_xpath error: nil] objectAtIndex: 0] stringValue];
            NSRange r = NSMakeRange (0, 3);
            if ([article_subject length] < 3) {
                r = NSMakeRange(0, [article_subject length]);
            }
            if ([[article_subject substringWithRange: r] isEqualToString: @"主题:"]) {
                article_subject = [article_subject stringByReplacingCharactersInRange:r withString: @""];
            }
            
    }
    
    @catch (NSException *exception) {
    }
    
    nodes = [doc nodesForXPath: article_nodes_xpath error: &error];
    if (error) {
        UCBREW_LOG(@"Error: %@\n\n", [error localizedDescription]);
    }
    NSMutableArray* articles_array = [NSMutableArray arrayWithCapacity: nodes.count];
    for (DDXMLElement* e in nodes) {
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
        NSString* author_id = nil;
        NSString* date = nil;
        NSString* article_id = nil;
        NSString* date_xpath = nil;
        NSString* author_xpath = nil;
        NSString* article_id_xpath = nil;

        if (self.viewMode == 2) {
            author_xpath = @"./div[@class='nav hl']/div[1]/a[2]";
            date_xpath = @"./div[@class='nav hl']/div[1]/a[3]";
            article_id_xpath = @"./div[@class='nav hl']/div[2]/a[1]/@href";
        } else if (self.viewMode == 0){
            author_xpath = @"./div[@class='nav hl']/div[1]/a[1]";
            date_xpath = @"./div[@class='nav hl']/div[1]/a[2]";
        }
        
        NSString* content = nil;
        NSMutableString* reply = nil;
        static NSString* content_xpath = @"./div[@class='sp']";
        @try {
            if (self.viewMode == 2) {
                @try {
                    NSString* article_post_path = [[[e nodesForXPath: article_id_xpath
                                                               error: nil] objectAtIndex: 0] stringValue];
                    NSString* article_post_regex = @"^\\/article\\/.+\\/post\\/(\\d+)$";
                    NSRange matchRange = NSMakeRange(NSNotFound, 0UL);
                    NSRange emptyRange = NSMakeRange(NSNotFound, 0UL);
                    if (!NSEqualRanges(matchRange = [article_post_path rangeOfRegex:article_post_regex capture:1],
                            emptyRange)) {
                        article_id = [article_post_path substringWithRange: matchRange];
                        UCBREW_LOG_OBJECT(article_id);
                    }
                }
                @catch (NSException* e){
                }
            }
            author_id = [[[e nodesForXPath: author_xpath error: nil] objectAtIndex: 0] stringValue];
            date = [[[e nodesForXPath: date_xpath error:nil] objectAtIndex: 0] stringValue];
            //content = [[[e nodesForXPath: content_xpath error: nil] objectAtIndex: 0] stringValue];
            //content = @"<img src=\"http://att.newsmth.net/nForum/att/MyPhoto/1842532/216/middle\"></img>";
            
            DDXMLElement* content_node = [[e nodesForXPath: content_xpath error: nil] objectAtIndex: 0];
            static NSString* inline_image_xpath = @".//img[@src]";
            NSArray* img_node_array = [content_node nodesForXPath: inline_image_xpath error: nil];
            for (DDXMLElement* img_node in img_node_array) {
                DDXMLNode* image_parent_node = [img_node parent];
                NSString* parent_tag_name = [image_parent_node name];
                if ([parent_tag_name isEqualToString: @"a"]) {
                    DDXMLElement* a_node = (DDXMLElement*) image_parent_node;
                    /*
                    for (NSUInteger i = 0; i < image_parent_child_count; i ++){
                        [[[image_parent_node childAtIndex: i] name] isEqualToString: @"a"];
                        [image_parent_node 
                    }
                     */
                    [a_node removeAttributeForName:@"href"];
                }
#ifdef UCSMTH_CONFIG_USE_FIX_WEBKIT_RELATION_IMAGE_URL
                if (self.showImage) {
                    NSString* image_href = [[img_node attributeForName: @"src"] stringValue];
                    if (!![image_href length]) {
                        /*
                         * fixed domain
                         */
                        NSURL* image_url = [NSURL URLWithString: image_href];
                        if (![image_url.host length]) {
                            NSString* new_href = [NSString stringWithFormat: @"http://m.newsmth.net%@", image_href];
                            DDXMLNode* href_attr = [DDXMLNode attributeWithName: @"src" stringValue: new_href];
                            [img_node removeAttributeForName: @"src"];
                            [img_node addAttribute: href_attr];
                        }
                    }
                } else {
                    [img_node removeAttributeForName: @"src"];
                    DDXMLNode* alt_attr = [DDXMLNode attributeWithName: @"alt"
                                                           stringValue: NSLocalizedString(@"Image Blocked", nil)];
                    [img_node addAttribute: alt_attr];
                }
                //[img_node removeAttributeForName:@"border"]; 
                //[img_node removeAttributeForName:@"title"]; 
                //[img_node removeAttributeForName:@"class"]; 
                //DDXMLNode* width_node = [DDXMLNode attributeWithName:@"width" stringValue: @"260"];
                //DDXMLNode* height_node = [DDXMLNode attributeWithName:@"height" stringValue:@"auto"];
                //[img_node addAttribute:width_node];
                //[img_node addAttribute:height_node];
#endif
            }
            // FIX photo
            content = [content_node XMLString] ;
        }
        @catch (NSException *exception) {
            [pool release];
            continue;
        }
        USArticle* article = [[[USArticle alloc] init] autorelease];
        article.artcileId = article_id;
        article.authorId = author_id;
        article.date = date;
        article.contentHTML = content;
        article.reply = reply.length ? reply : nil;
        article.attachments = nil;
        [articles_array addObject: article];
        [pool release];
    }
    self.articles = articles_array;
    self.articleSubject = article_subject;
}
@end
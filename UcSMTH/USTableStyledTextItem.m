//
//  USTableStyledTextItem.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-21.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USStyledTextTableItemCell.h"
#import "USTableStyledTextItem.h"

@implementation USTableStyledTextItem
- (Class) cellClass
{
    return [USStyledTextTableItemCell class];
}
@end

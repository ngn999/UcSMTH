//
// Created by kerberos on 12-8-19.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "USImage.h"
#import "Three20UI.h"


@implementation USImage
{

@private
    id <TTPhotoSource> _photoSource;
    NSString* _url;
    NSString* _caption;
    CGSize _size;
}
@synthesize photoSource = _photoSource;
@synthesize url = _url;
@synthesize caption = _caption;
@synthesize size = _size;


- (void) dealloc
{
    [_url release], _url = nil;
    [_caption release], _caption = nil;
    [super dealloc];
}


@end
//
//  TestStyledTextWithURLImageVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-22.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//
#import "UcBrewKit.h"
#import "TestStyledTextWithURLImageVC.h"

@interface TestStyledTextWithURLImageVC ()
@property (nonatomic, retain) TTStyledText* text;
@end

@implementation TestStyledTextWithURLImageVC
@synthesize text;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[TTURLCache sharedCache] removeURL: @"http://att.newsmth.net/nForum/att/MyPhoto/1842494/254" 
                               fromDisk: YES];
	// Do any additional setup after loading the view.
    NSString* test_html = @"ab<img src=\"http://att.newsmth.net/nForum/att/MyPhoto/1842494/254\"> </img>";
    text = [[TTStyledText textFromXHTML: test_html
             lineBreaks:YES URLs:YES] retain];
    text.font = [UIFont fontWithName:@"Helvetica" size:20.0f];
    text.width = 100;
    text.textAlignment = UITextAlignmentLeft;
    text.delegate = self;
    //[text layoutIfNeeded];
    UCBREW_LOG (@"%f:", text.height);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void) styledTextNeedsDisplay:(TTStyledText *) styledText
{
    UCBREW_CALL_METHOD;
    UCBREW_LOG(@"text changed: %f", styledText.height);
}
@end

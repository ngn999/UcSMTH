//
//  TestXBPageCurlVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-22.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "TestXBPageCurlVC.h"
#import "XBCurlView.h"

@interface TestXBPageCurlVC ()
@property (nonatomic, assign) BOOL isCurled;
@property (nonatomic, retain) XBCurlView* curlView;
@end

@implementation TestXBPageCurlVC
@synthesize curlView;
@synthesize isCurled;

+ (id) viewController
{
    TestXBPageCurlVC* vc = [[TestXBPageCurlVC alloc] initWithStyle: UITableViewStylePlain];
    return [vc autorelease];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear: animated];
    [self.navigationController setNavigationBarHidden: NO animated: YES];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear: animated];
    CGRect r = self.view.frame;
    self.curlView = [[[XBCurlView alloc] initWithFrame: r] autorelease];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear: animated];
    [self.navigationController setNavigationBarHidden: YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Curl" style:UIBarButtonItemStyleBordered target:self action: @selector(curlButtonAction:)] autorelease];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    return !isCurled;
}

- (void) createModel
{
    self.dataSource = [TTListDataSource dataSourceWithObjects:
                       [TTTableTextItem itemWithText:@"Cell1"],
                       [TTTableLongTextItem itemWithText: @"LongLonglonglongLonglonglongLonglonglongTExt"
                                               accessory: UITableViewCellAccessoryDisclosureIndicator],
                       nil];
}

- (void) curlButtonAction: (id) sender
{
    if (!isCurled) {
        CGRect r = self.tableView.frame;
        [self.curlView drawViewOnFrontOfPage:self.tableView];
        //[self.curlView drawImageOnNextPage:[UIImage imageNamed:@"appleStore"]];
        self.curlView.opaque = NO; //Transparency on the next page (so that the view behind curlView will appear)
        self.curlView.pageOpaque = YES; //The page to be curled has no transparency
        self.curlView.pageOpaque = NO; //The page to be curled has no transparency
        [self.curlView curlView:self.tableView
               cylinderPosition:CGPointMake(r.size.width/2, r.size.height/4)
                  cylinderAngle:M_PI/8.4 cylinderRadius:UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad? 160: 70 animatedWithDuration: 0.6f];
        isCurled = YES;
    } else {
        [self.curlView uncurlAnimatedWithDuration: 0.6];
        isCurled = NO;
    }
}


- (IBAction)uncurlButtonAction:(id)sender
{
    [self.curlView uncurlAnimatedWithDuration: 0.6];
    isCurled = NO;
}

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end

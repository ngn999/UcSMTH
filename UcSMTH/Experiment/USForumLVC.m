//
//  USFavoriteVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-23.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USBoardFeed.h"
#import "UcBrewKit.h"
#import "USBoardVC.h"
#import "FMDatabase.h"
#import "FMResultSet.h"
#import "UcSMTHEngine.h"
#import "USForumLVC.h"

@interface USForumLVC ()

@end

@implementation USForumLVC

+ (id) viewController
{
    USForumLVC* vc = [[USForumLVC alloc] initWithStyle: UITableViewStylePlain];
    return [vc autorelease];
}

- (id) initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle: style];
    if (self) {
        self.wantSearch = YES;
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSString*) title
{
    return NSLocalizedString (@"Forums", nil);
}

- (NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return (NSString*) [[self tableDataForTableView: tableView] objectForSectionAtIndex: section];
}

- (MwfTableData*) createAndInitTableData
{
    MwfTableData* table_data = [MwfTableData createTableDataWithSections];
    FMDatabase* database = [FMDatabase databaseWithPath: [[UcSMTHEngine sharedEngine] cacheFilePath]];
    if ([database open]) {
        FMResultSet* rset = [database executeQuery: @"SELECT name, id from section;"];
        while ([rset next]) {
            NSString* name = [rset stringForColumn: @"name"];
            NSString* bid = [rset stringForColumn: @"id"];
            FMResultSet* crs = [database executeQuery: @"SELECT name, id from board where section = ?" , bid];
            BOOL section_added = NO;
            NSUInteger si = 0;
            while ([crs next]) {
                if (!section_added) {
                    si = [table_data addSection: name];
                    section_added = YES;
                }
                NSString* board_name = [crs stringForColumn: @"name"];
                NSString* board_id = [crs stringForColumn: @"id"];
                USBoardFeed* feed = [[[USBoardFeed alloc] init] autorelease];
                feed.boardName = board_name;
                feed.boardId = board_id;
                [table_data addRow: feed inSection: si];
            }
        }
    }
    return table_data;
}

-$cellFor (USBoardFeed);
{
    static NSString * CellIdentifier = @"BoardCell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:CellIdentifier];
    }
    return cell;
}

-$configCell(USBoardFeed, UITableViewCell);
{
    cell.textLabel.text = item.boardName;
    cell.detailTextLabel.text = [NSString stringWithFormat: @"[ %@ ]", item.boardId];
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MwfTableData* table_data = [self tableDataForTableView: tableView];
    USBoardFeed* feed = [table_data objectForRowAtIndexPath: indexPath];
    USBoardVC* vc = [USBoardVC viewControllerWithBoardId: feed.boardId];
    [self.navigationController pushViewController: vc animated:YES];
}

- (NSArray*) sectionIndexTitlesForTableView:(UITableView *)tableView
{
    MwfTableData* table_data = [self tableDataForTableView: tableView];
    if (table_data == self.tableData) {
        /*
         * Normal mode
         */
        NSUInteger section_count = [table_data numberOfSections];
        NSMutableArray* array = [NSMutableArray arrayWithCapacity: section_count];
        for (NSUInteger i = 0; i < section_count; i++ ) {
            [array addObject: [table_data objectForSectionAtIndex: i]];
        }
        if (!! array.count) {
            return array;
        } else {
            return nil;
        }
    } else { /* search mode */
        return nil;
    }
}
#pragma mark -
#pragma search
- (MwfTableData*) createSearchResultsTableDataForSearchText:(NSString *)searchText scope:(NSString *)scope
{
    MwfTableData* table_data = [MwfTableData createTableDataWithSections];
    NSUInteger section_count = [self.tableData numberOfSections];
    for (NSUInteger i = 0;  i < section_count; i ++) {
        BOOL section_added = NO;
        NSUInteger row_count = [self.tableData numberOfRowsInSection: i];
        NSUInteger searched_section_index = 0;
        for (NSUInteger j = 0; j < row_count; j++ ) {
            id item = [self.tableData objectForRowAtIndexPath: [NSIndexPath indexPathForRow: j inSection: i]];
            USBoardFeed* feed = UCBREW_DYNAMIC_CAST(item, USBoardFeed);
            if (feed) {
                NSString* board_name = feed.boardName;
                NSString* board_id = feed.boardId;
#if 0
                NSComparisonResult result_name = [board_name compare: board_name
                                                             options: (NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch)
                                                               range: NSMakeRange(0, board_name.length)];
                NSComparisonResult result_id = [board_id compare: board_id
                                                         options: (NSCaseInsensitiveSearch | NSDiacriticInsensitiveSearch)
                                                           range: NSMakeRange(0, board_id.length)];
#endif
                NSRange result_name = [board_name rangeOfString: searchText];
                NSRange result_id = [board_id rangeOfString: searchText options:NSCaseInsensitiveSearch];
                if ( !!result_name.length || !!result_id.length ) {
                    if (!section_added) {
                        searched_section_index = [table_data addSection: [self.tableData objectForSectionAtIndex: i]];
                        section_added = YES;
                    }
                    [table_data addRow: feed inSection: searched_section_index];
                }
            }
        }
    }
    return table_data;
}
@end

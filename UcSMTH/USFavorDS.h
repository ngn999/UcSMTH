//
//  USFavorDS.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USListDataSource.h"
@class USFavorModel;

@interface USFavorDS : USListDataSource
@property (nonatomic, copy) NSString* dirName;
@property (nonatomic, copy) NSString* dirId;
@property (nonatomic, retain) USFavorModel* favorModel;

+ (id) dataSource;
+ (id) dataSourceWithDirName: (NSString*) dirName dirId: (NSString*) dirId;
@end

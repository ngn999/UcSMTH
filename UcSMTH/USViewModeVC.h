//
// Created by kerberos on 12-8-22.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "USTableViewController.h"

@class USTableViewController;
@class USViewModeSelectVC;

@protocol USViewModeVCDelegate <NSObject>
@optional
- (void) viewModeDidFinishSelect: (USViewModeSelectVC*) viewController mode: (NSInteger) mode;
- (void) viewModeDidCancelSelect: (USViewModeSelectVC*) viewController;
@end

@interface USViewModeVC : USTableViewController
@property (nonatomic, assign) id<USViewModeVCDelegate> delegate;

+ (id) viewControllerWithDelegate: (id<USViewModeVCDelegate>) delegate;
+ (id) viewControllerWithMode: (NSInteger) aMode delegate: (id<USViewModeVCDelegate>) delegate;
@end
//
// Created by kerberos on 12-8-19.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>

#import "TTPhoto.h"

@protocol TTPhotoSource;

@interface USImage : NSObject <TTPhoto>
@property (nonatomic, assign) id<TTPhotoSource> photoSource;
@property (nonatomic, copy) NSString* url;
@property (nonatomic, copy) NSString* caption;
@property (nonatomic, assign) CGSize size;
@end
//
//  USBoardModel.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "DDXML.h"
#import "DDXML+HTML.h"
#import "UcBrewKit.h"
#import "UcSMTHConfig.h"
#import "USURLRequest.h"
#import "USURLHtmlResponse.h"
#import "USPostFeedParser.h"
#import "USPostFeed.h"
#import "USBoardModel.h"

@interface USBoardModel ()
@property (nonatomic, readwrite, retain) NSMutableArray* topFeeds;
@property (nonatomic, readwrite, retain) NSMutableArray* postFeeds;
@property (nonatomic, assign) NSUInteger startIndexOfPage;
@property (nonatomic, copy) NSString* boardId;
@end

@implementation USBoardModel
@synthesize topFeeds;
@synthesize postFeeds;
@synthesize boardId;
@synthesize viewMode;
@synthesize totalOfPage;
@synthesize indexOfPage;
@synthesize startIndexOfPage;

+ (id) modelWithBoardId:(NSString *)aBoardId startIndexOfPage: (NSUInteger) aStartIndexOfPage viewMode:(NSUInteger)aViewMode
{
    USBoardModel* model = [[USBoardModel alloc] initWithBoardId: aBoardId startIndexOfPage: aStartIndexOfPage viewMode: aViewMode];
    return [model autorelease];
}

+ (id) modelWithBoardId:(NSString *)aBoardId startIndexOfPage: (NSUInteger) aStartIndexOfPage
{
    USBoardModel* model = [[USBoardModel alloc] initWithBoardId: aBoardId startIndexOfPage: aStartIndexOfPage];
    return [model autorelease];
}

- (id) initWithBoardId: (NSString*) aBoardId startIndexOfPage: (NSUInteger) aStartIndexOfPage viewMode: (NSUInteger) aViewMode;
{
    self = [super init];
    if (self) {
        self.boardId = aBoardId;
        self.viewMode = aViewMode;
        indexOfPage = 1;
        totalOfPage = 0;
        startIndexOfPage = aStartIndexOfPage;
    }
    
    return self;
}

- (id) initWithBoardId: (NSString*) aBoardId startIndexOfPage:(NSUInteger)aStartIndexOfPage
{
    self = [self initWithBoardId: aBoardId startIndexOfPage: 1 viewMode: 2];
    return self;
}

- (void) dealloc
{
    UCBREW_RELEASE (boardId);
    UCBREW_RELEASE  (postFeeds);
    UCBREW_RELEASE  (topFeeds);
    [super dealloc];
}

- (void) load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
    if (!self.isLoading && TTIsStringWithAnyText(boardId)) {
        if (more) {
            if (indexOfPage < totalOfPage) {
                indexOfPage ++;
            }
        } else {
            indexOfPage = startIndexOfPage;
            totalOfPage = 0;
            self.postFeeds = [NSMutableArray arrayWithCapacity: 0];
            self.topFeeds = [NSMutableArray arrayWithCapacity: 0];
        }
        NSString* request_url = nil;
        request_url = [NSString stringWithFormat:@"http://m.newsmth.net/board/%@/%d/?p=%d", self.boardId, self.viewMode, self.indexOfPage];
        //request_url = @"http://m.newsmth.net/board/MyPhoto";
        UCBREW_LOG_OBJECT(request_url);
        USURLRequest* req = [USURLRequest requestWithURL: request_url delegate:self];
        USURLHtmlResponse* resp = [USURLHtmlResponse response];
        req.cachePolicy = TTURLRequestCachePolicyNetwork;
        [req.headers setObject:@"deflate\r\n" forKey:@"Accept-Encoding"];
        req.response = resp;
        [req send];
    }
}

- (void)requestDidFinishLoad:(TTURLRequest *)request
{
    USURLHtmlResponse* resp = (USURLHtmlResponse*) request.response;
    [self parseHtmlDocument: resp.htmlDocument];
    [super requestDidFinishLoad:request];
}

- (void) parseHtmlDocument: (DDXMLDocument*) doc
{
    NSError* err = nil;
    NSArray* nodes = nil;
    
    NSString* totalPageHref = nil;
    NSString* postIdHref = nil;
    NSString* postSubject = nil;
    NSString* postAuthorHref = nil;
    NSString* postDate = nil;
#ifdef UCSMTH_CONFIG_USE_PARSE_PAGING_USE_PARSEKIT
    NSString* boardPageXPath = @"//div[@id='m_main']/div[@class='sec nav']/form/a[starts-with(text(), '尾页')]";
    nodes = [doc nodesForXPath:boardPageXPath error:&err];
    for (DDXMLElement* e in nodes) {
        totalPageHref = [[e attributeForName:@"href"] stringValue];
    }
#else
    NSString* boardPageXPath = @"//div[@id='m_main']/div[@class='sec nav']/form/a[@class='plant'][1]";
    nodes = [doc nodesForXPath:boardPageXPath error:&err];
    for (DDXMLElement* e in nodes) {
        totalPageHref = [e stringValue];
    }
#endif
    
    NSString* xpath = @"//div[@id='m_main']/ul[@class='list sec']//li";
    NSString* articleXPath = @"div[1]/a[@href]";
    NSString* authorXPath = @"div[2]/a[@href][1]";
    nodes = [doc nodesForXPath:xpath error:nil];
    for (DDXMLElement* e in nodes) {
        NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
        @try {
            DDXMLElement* node = nil;
            node = [[e nodesForXPath:articleXPath error:nil] objectAtIndex:0];
            postSubject = node.stringValue;
            postIdHref = [[node attributeForName:@"href"] stringValue];
            BOOL isTopFeed = NO;
            NSString* aClass = [[node attributeForName:@"class"] stringValue];
            if (!!aClass && [aClass isEqualToString:@"top"]) {
                isTopFeed = YES;
            }
            postDate = [[[[e nodesForXPath:@"div[2]/text()[1]" error:nil] objectAtIndex:0] stringValue] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            node = [[e nodesForXPath:authorXPath error:nil] objectAtIndex:0];
            postAuthorHref = [[node attributeForName:@"href"] stringValue];
            
            USPostFeedParserItem* item = [[USPostFeedParser sharedParser] parsePostWithPageHref: totalPageHref
                                                                                   withPostHref: postIdHref
                                                                                 withAuthorHref: postAuthorHref];
            
            if (item) {
                USPostFeed* feed = [[[USPostFeed alloc] init] autorelease];
                feed.postId = item.postId;
                feed.postAuthorId = item.postAuthor;
                feed.postTitle = postSubject;
                feed.postDate = postDate;
                feed.postBoardId = self.boardId;
                totalOfPage = item.totalPage;
                if (isTopFeed) {
                    [self.topFeeds addObject: feed];
                } else {
                    [self.postFeeds addObject: feed];
                }
            }
        }
        @catch (NSException *exception) {
            NSLog(@"Parse error in %s", __PRETTY_FUNCTION__);
        }
        [pool release];
    }
    //NSLog(@"count in set: %d", self.articleSet.count);
}
@end
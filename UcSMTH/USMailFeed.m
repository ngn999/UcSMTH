//
//  USMailFeed.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USMailFeed.h"

@implementation USMailFeed
@synthesize mailId;
@synthesize mailAuthor;
@synthesize mailSubject;
@synthesize mailDate;

- (void) dealloc
{
    UCBREW_RELEASE (mailDate);
    UCBREW_RELEASE (mailSubject);
    UCBREW_RELEASE (mailAuthor);
    UCBREW_RELEASE (mailId);
    [super dealloc];
}
@end

//
//  USPostFeedParser.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <Foundation/Foundation.h>

////////////////////////////////////////////////////////////////////////////////
@interface USPostFeedParserItem : NSObject
@property (nonatomic, copy) NSString* postId;
@property (nonatomic, copy) NSString* postAuthor;
@property (nonatomic, assign) NSUInteger totalPage;

@end

////////////////////////////////////////////////////////////////////////////////
@interface USPostFeedParser : NSObject
+ (id) sharedParser;
- (USPostFeedParserItem*) parsePostWithPageHref: (NSString*) pageHref
                                   withPostHref: (NSString*) postHref
                                 withAuthorHref: (NSString*) authorHref;
@end

//
//  USHtmlRequestModel.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USHtmlRequestModel.h"
#import "USURLHtmlResponse.h"

////////////////////////////////////////////////////////////////////////////////
@interface USHtmlRequestModel ()
@property (nonatomic, retain) id<USModelAuthChecker> modelAuthChecker;

@end
////////////////////////////////////////////////////////////////////////////////

@implementation USHtmlRequestModel
@synthesize modelAuthChecker;

- (id) init
{
    self = [super init];
    if (self) {
        self.modelAuthChecker = [[[USModelAuthChecker alloc] init] autorelease];
    }
    
    return self;
}

- (void) dealloc
{
    UCBREW_RELEASE (modelAuthChecker);
    [super dealloc];
}

- (id<USModelAuthChecker>) checker
{
    return self.modelAuthChecker;
}

- (NSError*) isNeedAuthForHtmlDocument: (DDXMLDocument*) htmlDocument
{
    return [[self checker] authCheckHtmlDocument: htmlDocument];
}

- (void) requestDidFinishLoad:(TTURLRequest *)request
{
    NSError* error = nil;
    if ([request.response isKindOfClass:[USURLHtmlResponse class]]) {
        error = [self isNeedAuthForHtmlDocument: [(USURLHtmlResponse*)request.response htmlDocument]];
    }
    
    if (!! error) {
        [self request: request didFailLoadWithError: error];
    } else {
        [super requestDidFinishLoad: request];
    }
}

- (NSError*) authCheckHtmlDocument:(DDXMLDocument *)htmlDocument
{
    return nil;
}
@end
////////////////////////////////////////////////////////////////////////////////
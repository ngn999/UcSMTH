//
//  USMailFeed.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface USMailFeed : NSObject
@property (nonatomic, copy) NSString* mailId;
@property (nonatomic, copy) NSString* mailAuthor;
@property (nonatomic, copy) NSString* mailSubject;
@property (nonatomic, copy) NSString* mailDate;
@end

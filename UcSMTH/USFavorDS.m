//
//  USFavorDS.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USBoardFeed.h"
#import "USFavorModel.h"
#import "USFavorDS.h"

@implementation USFavorDS
{
@private
    NSString* _dirName;
    NSString* _dirId;
}

@synthesize favorModel;
@synthesize dirName = _dirName;
@synthesize dirId = _dirId;


+ (id) dataSource
{
    USFavorDS* ds = [[USFavorDS alloc] init];
    return [ds autorelease];
}

+ (id) dataSourceWithDirName: (NSString*) dirName dirId: (NSString*) dirId
{
    USFavorDS* ds = [[self alloc] initWithDirName: dirName dirId: dirId];
    return [ds autorelease];
}

- (id) init
{
    self = [super init];
    if (self) {
        self.favorModel = [[[USFavorModel alloc] init] autorelease];
    }
    
    return self;
}

- (id) initWithDirName: (NSString*) dirName dirId: (NSString*) dirId
{
    self = [super init];
    if (self) {
        _dirId = [dirId copy];
        _dirName = [dirName copy];

        self.favorModel = [[[USFavorModel alloc] initWithDirName: dirName dirId: dirId] autorelease];
    }

    return self;
}

- (void) dealloc
{
    UCBREW_RELEASE(favorModel);
    [_dirName release], _dirName = nil;
    [_dirId release], _dirId = nil;
    [super dealloc];
}

- (id<TTModel>) model
{
    return self.favorModel;
}

- (NSString*) titleForLoading:(BOOL)reloading
{
    if (reloading) {
        return NSLocalizedString (@"Updating favorites ...", nil);
    } else {
        return NSLocalizedString (@"Loading favorites ...", nil);
    }
}

- (NSString*) titleForEmpty
{
    return NSLocalizedString (@"No favorites.", nil);
}

- (NSString*) subtitleForError:(NSError *)error
{
    return [error localizedDescription];
}

- (NSString*) titleForError:(NSError *)error
{
    return [super subtitleForError: error];
}

- (void) tableViewDidLoadModel:(UITableView *)tableView
{
    NSMutableArray* ma = [NSMutableArray arrayWithCapacity: self.favorModel.boardFeeds.count];
    for (USBoardFeed* feed in self.favorModel.boardFeeds) {
        UTTableLongSubtitleItem* item = nil;
        if (feed.isSection) {
            item = [UTTableLongSubtitleItem itemWithText: feed.boardName
                                                                         subtitle: @"[目录]"
                                                                        accessory: UITableViewCellAccessoryDisclosureIndicator];
            item.userInfo = feed;
        } else {
            item = [UTTableLongSubtitleItem itemWithText: feed.boardName
                                                                 subtitle: [NSString stringWithFormat: @"[%@]", feed.boardId]
                                                                accessory: UITableViewCellAccessoryDisclosureIndicator];
            item.userInfo = feed;
        }
        [ma addObject:item];
    }
    
    self.items = ma;
}
@end
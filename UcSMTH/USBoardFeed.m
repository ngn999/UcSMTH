//
//  USBoardFeed.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USBoardFeed.h"

@implementation USBoardFeed
@synthesize boardId;
@synthesize boardName;
@synthesize isSection;

- (void) dealloc
{
    UCBREW_RELEASE (boardId);
    UCBREW_RELEASE (boardName);
    [super dealloc];
}

@end

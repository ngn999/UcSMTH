//
//  USPostFeed.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "USPostFeed.h"

@implementation USPostFeed
@synthesize postId;
@synthesize postTitle;
@synthesize postDate;
@synthesize postAuthorId;
@synthesize postBoardId;
@synthesize isTop;

- (id) copyWithZone:(NSZone *)zone
{
    USPostFeed* newFeed = [[[USPostFeed class] allocWithZone:zone] init];
    newFeed.postAuthorId = self.postAuthorId;
    newFeed.postTitle = self.postTitle;
    newFeed.postDate = self.postDate;
    newFeed.postBoardId = self.postBoardId;
    newFeed.postId  = self.postId;
    newFeed.isTop = self.isTop;
    
    return newFeed;
}

- (void) dealloc
{
    UCBREW_RELEASE (postBoardId);
    UCBREW_RELEASE (postAuthorId);
    UCBREW_RELEASE (postDate);
    UCBREW_RELEASE (postTitle);
    UCBREW_RELEASE (postId);
    [super dealloc];
}
@end
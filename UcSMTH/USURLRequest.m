//
//  USURLRequest.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USURLRequest.h"

@implementation USURLRequest

+ (id) requestWithURL:(NSString *)url delegate:(id)delegate
{
    USURLRequest* req = [[USURLRequest alloc] initWithURL: url delegate: delegate];
    req.timeoutInterval = 30;
    return [req autorelease];
}
                        
- (id) initWithURL:(NSString *)URL delegate:(id)delegate
{
    self = [super initWithURL:URL delegate:delegate];
    if (self) {
    }
    return self;
}
@end

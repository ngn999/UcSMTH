//
//  USMailboxDS.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "TTListDataSource.h"

@interface USMailboxDS : TTListDataSource
+ (id) dataSourceWithBoxId: (NSString*) aBoxId startIndexOfPage: (NSUInteger) aStartIndexOfPage;
@end

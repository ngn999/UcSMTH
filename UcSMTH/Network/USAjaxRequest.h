//
// Created by kerberos on 12-8-25.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "ASIFormDataRequest.h"

@interface USAjaxRequest : ASIFormDataRequest
+ (id) requestWithURL: (NSString*) aURL delegate: (id) aDelegate;
+ (id) requestWithURLString: (NSString*) aURLString;
@end
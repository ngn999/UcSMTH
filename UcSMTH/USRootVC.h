//
//  USRootVC.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-7-3.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#define CK_SHORTHAND
#import "MwfTableViewController.h"

@interface USRootVC : MwfTableViewController
+ (id) viewController;
+ (id) viewControllerWithStyle: (UITableViewStyle) style;
@end

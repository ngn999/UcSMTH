//
//  USSettingsVC.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "USTableViewController.h"

@interface USSettingsVC : USTableViewController
+ (id) viewController;
@end

//
// Created by kerberos on 12-8-27.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import "NFMessageComposerVC.h"

typedef enum {
    USArticleComposerModeNone = 0,
    USArticleComposerModeNew = 1,
    USArticleComposerModeEdit = 2,
    USArticleComposerModeReply = 3
} USArticleComposerMode;

@interface USArticleComposer : NFMessageComposerVC
@property (nonatomic, retain) NSString* boardId;
@property (nonatomic, retain) NSString* articleId;

+ (id) composerWithMode: (USArticleComposerMode) mode;
@end
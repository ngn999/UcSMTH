//
//  USMailboxVC.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-28.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "UcSMTHEngine.h"
#import "USMailFeed.h"
#import "USMailboxModel.h"
#import "USNavigationController.h"
#import "USMailComposer.h"
#import "USMailWebVC.h"
#import "USMailboxDS.h"
#import "USMailboxVC.h"
#import "USPageVC.h"
#import "USTableMoreButton.h"

@interface USMailboxVC () <USPageVCDelegate, NFMessageComposerVCDelegate>
@property (nonatomic, copy) NSString* boxId;
@property (nonatomic, assign) NSUInteger startIndexOfPage;
@property (nonatomic, assign) BOOL isPaged;
@end

@implementation USMailboxVC
@synthesize isPaged;
@synthesize startIndexOfPage;
@synthesize boxId;

+ (id) viewControllerWithBoxId:(NSString *)aBoxId
{
    USMailboxVC* vc = [[USMailboxVC alloc] initWithStyle: UITableViewStylePlain boxId: aBoxId];
    return [vc autorelease];
}

- (id) initWithStyle:(UITableViewStyle)style boxId: (NSString*) aBoxId
{
    self = [super initWithStyle: style];
    if (self) {
        self.boxId = aBoxId;
        self.startIndexOfPage = 1;
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.variableHeightRows = YES;
    UIBarButtonItem* compose_item = nil;
    compose_item = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemCompose
                                                                target: self
                                                                action: @selector(handleComposeAction)]
                  autorelease];
    self.navigationItem.rightBarButtonItem = compose_item;
}


- (void) dealloc
{
    UCBREW_RELEASE(boxId);
    [super dealloc];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSString*) title
{
    if ([self.boxId isEqualToString: @"inbox"]) {
        return NSLocalizedString(@"Inbox", nil);
    } else if ([self.boxId isEqualToString: @"outbox"]) {
        return NSLocalizedString(@"Sent", nil);
    } else if ([self.boxId isEqualToString: @"deleted"]) {
        return NSLocalizedString(@"Deleted", nil);
    }
    return NSLocalizedString(@"Unknown", nil);
}

- (void) createModel
{
    self.dataSource = [USMailboxDS dataSourceWithBoxId: self.boxId startIndexOfPage: self.startIndexOfPage];
}

- (void) controller:(USPageVC *)controller didFinishPaging:(NSUInteger)page
{
    UCBREW_CALL_METHOD;
    UCBREW_LOG(@"paging to: %d", page);
    [self.navigationController popToViewController: self animated: YES];
    /*
     * Loading new model
     */
    self.startIndexOfPage = page;
    [self invalidateModel];
    //[m loadWithStartIndexOfPage: page];
}

- (void) didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath
{
    UCBREW_LOG_OBJECT(object);
    UTTableLongSubtitleItem* item = UCBREW_DYNAMIC_CAST(object, UTTableLongSubtitleItem);
    
    USMailFeed* feed = UCBREW_DYNAMIC_CAST(item.userInfo, USMailFeed);
    if (UcBrew_IsNotEmptyString(feed.mailId)
        && UcBrew_IsNotEmptyString(self.boxId)) {
        USMailWebVC* vc = [USMailWebVC viewControllerWithBoxId: self.boxId mailId: feed.mailId];
        [self.navigationController pushViewController: vc animated: YES];
    }
}

- (void) accessoryButtonTappedForObject: (id) object atIndexPath: (NSIndexPath*) indexPath
{
    USTableMoreButton* button = UCBREW_DYNAMIC_CAST(object, USTableMoreButton);
    if (button) {
        USMailboxModel* model = (USMailboxModel*)self.model;
        USPageVC* vc = [USPageVC viewControllerWithIndexOfPage: self.startIndexOfPage totalOfPage:model.totalOfPage
                                                      delegate: self];
        [self.navigationController pushViewController: vc
                                             animated: YES];
    }
}

- (id) createDelegate
{
    TTTableViewDragRefreshDelegate* d = nil;
    d = [[[TTTableViewDragRefreshDelegate alloc] initWithController: self]
         autorelease];
    return d;
}

- (void) handleComposeAction
{
    USMailComposer* vc = [USMailComposer composerWithMode: USMailComposerModeNew];
    vc.delegate = self;
    vc.boxId = self.boxId;
    /*
    USMailComposerVC* vc = [USMailComposerVC viewControllerWithMailId: nil
                                                               userId: nil
                                                              subject: nil
                                                                boxId: nil
                                                             delegate: self];
    */
    USNavigationController* nav = [[[USNavigationController alloc] initWithRootViewController: vc] autorelease];
    [self presentModalViewController: nav animated: YES];
}

- (void) messageComposerDidCancel: (NFMessageComposerVC*) composerVC
{
    [self dismissModalViewControllerAnimated: YES];
}

- (void) messageComposerDidFinish: (NFMessageComposerVC*) composerVC
{
    [self dismissModalViewControllerAnimated: YES];
    UcSMTHEngine* engine = [UcSMTHEngine sharedEngine];
    if ([self.boxId isEqualToString: kUSMailComposerOutbox]
        && [[engine userDefaults] boolForKey:  @"postrefresh"]
        && self.startIndexOfPage == 1) {
        [self invalidateModel];
        [self invalidateView];
    }
}
@end
//
//  UcBrewCore.h
//  UcBrewKit
//
//  Created by Kerberos Zhang on 12-6-17.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <TargetConditionals.h>  
////////////////////////////////////////////////////////////////////////////////
#define UCBREW_DOMAIN @"ucbrew.com"
#define UCBREW_RELEASE(ivar) {\
        [ivar release];\
        ivar = nil; \
    }

#define UCBREW_I18N(string) NSLocalizedString((string), nil)

////////////////////////////////////////////////////////////////////////////////
#ifdef DEBUG
#define UCBREW_LOG(fmt, ...) do {\
        NSString* filepath = [NSString stringWithFormat: @"%s", __FILE__];\
        NSLog ((@"%@(%d) " fmt), [filepath lastPathComponent], __LINE__, ##__VA_ARGS__);\
        } while (0)
#else
#define UCBREW_LOG(fmt, ...) do {\
        } while (0)
#endif

#if TARGET_IPHONE_SIMULATOR
#define UCBREW_ASSERT(xx) {\
        if (!(xx)) {\
            UCBREW_LOG (@"UCBREW_ASSERT failed: %s", #xx);\
            __asm__("int $3\n" : : );\
        }\
    } ((void)0)
#else
#define UCBREW_ASSERT(xx) {\
        if (!(xx)) {\
            UCBREW_LOG (@"UCBREW_ASSERT failed: %s", #xx);\
        }\
    } ((void)0)
#endif

////////////////////////////////////////////////////////////////////////////////
#define UCBREW_LOG_OBJECT(object) UCBREW_LOG(@"%s: %@", #object, object)
#define UCBREW_DYNAMIC_CAST(object, klass) ([(object) isKindOfClass: [klass class]] ? (klass*) object : nil)
#define UCBREW_CALL_METHOD UCBREW_LOG (@"====%s====", __PRETTY_FUNCTION__)
#define UCBREW_ENTER_METHOD UCBREW_LOG (@"====%s==== Enter", __PRETTY_FUNCTION__)
#define UCBREW_LEAVE_METHOD UCBREW_LOG (@"====%s==== Leave", __PRETTY_FUNCTION__)

#define UCBREW_OBJ_RETAINCOUNT(obj) UCBREW_LOG (@"%d", [(obj) retainCount])
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
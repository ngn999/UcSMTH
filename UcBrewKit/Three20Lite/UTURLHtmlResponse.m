//
//  UTURLHtmlResponse.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-18.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "DDXML.h"
#import "DDXML+HTML.h"
#import "Three20Lite.h"
#import "UcBrewCore.h"
#import "UTURLHtmlResponse.h"

////////////////////////////////////////////////////////////////////////////////
@interface UTURLHtmlResponse ()

@property (nonatomic, retain, readwrite) DDXMLDocument* htmlDocument;

@end
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
@implementation UTURLHtmlResponse
@synthesize htmlDocument;

- (void) dealloc
{
    TT_RELEASE_SAFELY(htmlDocument);
    [super dealloc];
}

- (NSError*)request:(TTURLRequest *)request processResponse:(NSHTTPURLResponse *)response data:(id)data
{
    NSError* error = nil;
    TTDASSERT([data isKindOfClass:[NSData class]]);
    TTDASSERT(data != nil);
    
    DDXMLDocument* doc = [[DDXMLDocument alloc] initWithHTMLData:data
                                                         options:HTML_PARSE_NOWARNING | HTML_PARSE_NOERROR
                                                           error:&error];
    self.htmlDocument = doc;
    [doc release];
    return error;
}
@end
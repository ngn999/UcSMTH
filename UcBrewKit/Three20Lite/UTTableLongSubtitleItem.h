//
//  UTTableLongSubtitleItem.h
//  UcBrewKit
//
//  Created by Kerberos Zhang on 12-6-15.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "Three20Lite.h"

@interface UTTableLongSubtitleItem : TTTableTextItem
@property (nonatomic, copy) NSString* subtitle;
@property (nonatomic, assign) NSInteger maxTextLine;

+ (id)itemWithText: (NSString*)text
          subtitle: (NSString*)subtitle;

+ (id)itemWithText: (NSString*)text
          subtitle: (NSString*)subtitle
       maxTextLine: (NSInteger)textLine;

+ (id)itemWithText: (NSString*)text
          subtitle: (NSString*)subtitle
               URL: (NSString*)URL;

+ (id)itemWithText: (NSString*)text
          subtitle: (NSString*)subtitle
         accessory: (UITableViewCellAccessoryType)accessoryType;

+ (id)itemWithText: (NSString*)text
          subtitle: (NSString*)subtitle
       maxTextLine: (NSInteger)textLine
         accessory: (UITableViewCellAccessoryType)accessoryType;

+ (id)itemWithText: (NSString*)text
          subtitle: (NSString*)subtitle
               URL: (NSString*)URL
      accessoryURL: (NSString*)accessoryURL;

+ (id)itemWithText: (NSString*)text
          subtitle: (NSString*)subtitle
               URL: (NSString*)URL
      accessoryURL: (NSString*)accessoryURL;
@end

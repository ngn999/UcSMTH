//
//  UTTableMoreButton.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-19.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UTTableMoreButton.h"
#import "UTTableMoreButtonCell.h"
@implementation UTTableMoreButton
+ (id) itemWithText:(NSString *)text
{
    UTTableMoreButton* item = [[UTTableMoreButton alloc] init];
    item.text = text;
    return [item autorelease];
}

- (Class) cellClass
{
    return [UTTableMoreButtonCell class];
}


@end
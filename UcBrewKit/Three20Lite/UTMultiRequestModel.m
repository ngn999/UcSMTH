//
//  UTMultiRequestModel.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-19.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewKit.h"
#import "UTMultiRequestModel.h"

@interface UTMultiRequestModel ()
@property (nonatomic, retain) NSMutableArray* loadingRequests;
@property (nonatomic, assign) BOOL isLoaded;
@property (nonatomic, retain) NSError* lastError;
@end

@implementation UTMultiRequestModel
@synthesize loadingRequests;
@synthesize isLoaded;
@synthesize lastError;
@synthesize isCancelAll;

- (id) init
{
    self = [super init];
    if (self) {
        self.loadingRequests = [NSMutableArray arrayWithCapacity:1];
        self.isLoaded = NO;
    }
    
    return self;
}

- (void) dealloc
{
    UCBREW_RELEASE(loadingRequests);
    [super dealloc];
}

- (BOOL) isLoading
{
    return !! self.loadingRequests.count;
}

- (BOOL) isLoadingMore
{
    return NO;
}

- (BOOL) isOutdated
{
    return NO;
}

- (void) load:(TTURLRequestCachePolicy)cachePolicy more:(BOOL)more
{
}

- (void) cancel
{
    if (self.isLoading) {
        for (TTURLRequest* r in [[self.loadingRequests copy] autorelease]) {
            [r cancel];
        }
    } else {
        [self didCancelLoad];
    }
}

#pragma mark -
#pragma TTURLRequest delegate

- (void) requestDidStartLoad:(TTURLRequest *)request
{
    /*
     * keep request in array
     */
    if (!self.isLoading) {
        self.lastError = nil;
        [self didStartLoad];
    }
    [self.loadingRequests addObject: request];
}

- (void) requestDidCancelLoad:(TTURLRequest *)request
{
    /*
     * remove
     */
    [self.loadingRequests removeObject: request];
    if (!self.isLoading) {
        if (!self.lastError) {
            [self didCancelLoad];
        } else {
            [self didFailLoadWithError: self.lastError];
        }
    }
}

- (void) requestDidFinishLoad:(TTURLRequest *)request
{
    /*
     * complete one request
     */
    [self.loadingRequests removeObject: request];
    if (!self.loadingRequests.count) {
        self.isLoaded = YES;
        [self didFinishLoad];
    }
}

- (void) request:(TTURLRequest *)request didFailLoadWithError:(NSError *)error
{
    self.lastError = error;
    [self.loadingRequests removeObject: request];
    
    if (isCancelAll) {
        for (TTURLRequest* r in [[self.loadingRequests copy] autorelease]) {
            if (r.isLoading) {
                [r cancel];
            }
        }
    }
}
@end
//
//  UTTableMoreButton.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-19.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "TTTableMoreButton.h"

@interface UTTableMoreButton : TTTableMoreButton
+ (id) itemWithText:(NSString *)text;
- (Class) cellClass;
@end

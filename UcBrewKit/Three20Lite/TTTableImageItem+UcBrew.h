//
//  TTTableImageItem+UcBrew.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-17.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "TTTableImageItem.h"

@interface TTTableImageItem (UcBrew)

+ (id)itemWithText:(NSString*)text
          imageURL:(NSString*)imageURL
         accessory: (UITableViewCellAccessoryType) accessoryType;

@end

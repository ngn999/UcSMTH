//
//  UcHTMLView.h
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-22.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UcHTMLView : UIView
@property (nonatomic, copy) NSString* fontFamily;
@property (nonatomic, assign) CGFloat fontSize;
@property (nonatomic, assign) NSUInteger maxPortraitWidth;
@property (nonatomic, assign) NSUInteger maxLandscapeWidth;
@property (nonatomic, copy) NSString* addtionalStyle;
@property (nonatomic, readonly) UIScrollView* scrollView;

- (void) loadHTML: (NSString*) htmlSource baseUrl: (NSString*) baseUrl;
@end

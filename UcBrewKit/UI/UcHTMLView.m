//
//  UcHTMLView.m
//  UcSMTH
//
//  Created by Kerberos Zhang on 12-6-22.
//  Copyright (c) 2012年 UcBrew. All rights reserved.
//

#import "UcBrewCore.h"
#import "UcHTMLView.h"
static NSString* uc_html_default_style_format = @""
@"<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0; user-scalable=0; minimum-scale=1.0; maximum-scale=1.0\"/>"
@"<style type=\"text/css\">"
    @"body {"
        @"margin:0;"
        @"padding:9px;"
        @"font-family:\"%@\";"
        @"font-size:%f;"
        @"word-wrap:break-word;"
        @"-webkit-text-size-adjust:none;"
    @"}"
    @""
    @"img,video,iframe {"
        @"margin:5px 0 5px 0;"
    @"}"
    @""
    @"* {"
        @"height:auto;"
    @"}"
    @""
    @"@media (orientation:portrait) {"
        @"* {"
            @"max-width:%.0fpx;"
        @"}"
    @"}"
    @""
    @"@media (orientation:landscape) {"
        @"* {"
            @"max-width:%.0fpx;"
        "}"
    @"}"
    @"%@"
@"</style>"
@"";

@interface UcHTMLView () <UIWebViewDelegate>
@property (nonatomic, retain) UIWebView* webView;
@end

@implementation UcHTMLView
@synthesize webView;
@synthesize maxPortraitWidth;
@synthesize maxLandscapeWidth;
@synthesize fontFamily;
@synthesize fontSize;
@synthesize addtionalStyle;

+ (void) hackRemoveBackgroundFromWebView: (UIWebView*) webView
{
    for (UIView* subView in [webView subviews]) {
        if ([subView isKindOfClass:[UIScrollView class]]) {
            for (UIView* shadowView in [subView subviews]) {
                if ([shadowView isKindOfClass:[UIImageView class]]) {
                    [shadowView setHidden:YES];
                }
            }
        }
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
#if 1
        // Initialization code
        UIWebView* web_view = [[UIWebView alloc] initWithFrame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
       self.webView = web_view;
        [web_view release];
           
#endif
        self.webView.backgroundColor = [UIColor clearColor];
        self.webView.opaque = NO;
        self.webView.delegate = self;
        self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.webView.scalesPageToFit = NO;
        self.webView.allowsInlineMediaPlayback = NO;
        self.webView.mediaPlaybackRequiresUserAction = NO;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            self.maxPortraitWidth = 320;
            self.maxLandscapeWidth = 480;
        } else {
            self.maxPortraitWidth = 768;
            self.maxLandscapeWidth = 1024;
        }
        self.fontFamily = @"Helvetica";
        self.fontSize = 20.0f;
        self.addtionalStyle = @"";
        //[UcHTMLView hackRemoveBackgroundFromWebView: self.webView];
        [self addSubview:self.webView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (UIScrollView*) scrollView
{
#if 0
    for (id subview in self.webView.subviews) {
        if ([[subview class] isSubclassOfClass: [UIScrollView class]]) {
            return subview;
        }
    }    
#endif
    return nil;
}

- (void) dealloc
{
    UCBREW_CALL_METHOD;
    UCBREW_RELEASE(addtionalStyle);
    UCBREW_RELEASE(fontFamily);
#if 0
    UCBREW_RELEASE(webView);
#endif
    [super dealloc];
}

- (void) loadHTML:(NSString *)htmlSource baseUrl:(NSString *)baseUrl
{
    NSString* default_style = [NSString stringWithFormat: uc_html_default_style_format,
                                self.fontFamily, self.fontSize,
                                self.maxPortraitWidth, self.maxLandscapeWidth,
                               self.addtionalStyle];
    NSString* real_html = [NSString stringWithFormat:@"<html><head>%@</head><body>%@</body></html>", default_style, htmlSource];
#if 0
    NSURL* base_url = [NSURL URLWithString: baseUrl];
    [self.webView loadHTMLString: real_html baseURL: base_url];
#endif 
}

#pragma mark -
#pragma mark UIWebViewDelegate

- (BOOL) webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType 
{
    UCBREW_CALL_METHOD;
    return YES;
}

- (void) webViewDidFinishLoad:(UIWebView *)webView
{
    UCBREW_CALL_METHOD;
}
@end